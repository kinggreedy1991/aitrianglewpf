﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;

namespace AITriangleWPF
{
    /// <summary>
    /// Interaction logic for ProfilesandSettingsForm.xaml
    /// </summary>
    public partial class ProfilesandSettingsForm : Window
    {
        public int original = 1;
        private int FormType;
        private int FormMode = 0;
        string[] FormExpressions;
        string[] btnModeFormattedString = new string[2] { "Normal", "Debug" };
        public ProfilesandSettingsForm(int type,string[] expressions)
        {
            string temp = "";
            FormType = type;
            FormExpressions = expressions;
            InitializeComponent();
            btnMode.Content = btnModeFormattedString[FormMode];
            if (type == 0) this.Title = "Settings"; else this.Title = "Profiles";
            foreach (string str in expressions)
            {
                temp += str;
                temp += System.Environment.NewLine;
            }
            textBoxOutput.Text = temp;
            dataGridExpression.ItemsSource = LoadCollectionData();
        }
        public string[] getExpressions()
        {
            return FormExpressions;
        }
        private void btnMode_Click(object sender, RoutedEventArgs e)
        {
            FormMode = (FormMode + 1) % 2;
            btnMode.Content = btnModeFormattedString[FormMode];
            if (FormMode == 1)
            {
                borderDebugText.Visibility = System.Windows.Visibility.Visible;
                NormalModeGrid.Visibility = System.Windows.Visibility.Hidden;
                SaveCollectionData();
            }
            else
            {
                borderDebugText.Visibility = System.Windows.Visibility.Hidden;
                NormalModeGrid.Visibility = System.Windows.Visibility.Visible;
                dataGridExpression.ItemsSource = LoadCollectionData();
            }
            
        }
        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            string help = "";
            help += "Cách đặt tên biến: " + System.Environment.NewLine;
            help += "      + Biến là 1 ký tự: có thể viết liên tục mà không cần nêu thêm chi tiết, ví dụ: a, b, c" + System.Environment.NewLine;
            help += "      + Biến là 2 ký tự trở lên: phải dùng ngoặc nhọn đối với tên biến, ví dụ: {hA} là một biến, hA là 2 biến h * A" + System.Environment.NewLine;
            help += "      + Tên biến mặc định: a,b,c (cạnh); A,B,C (góc); mA,mB,mC (3 đường trung tuyến); hA,hB,hC (3 đường cao); pA,pB,pC (3 đường phân giác); p - nửa chu vi; S - diện tích; R - bán kính đường tròn ngoại tiếp; r - bán kính đường tròn nội tiếp" + System.Environment.NewLine;
            help += "      + Tên hằng: pi,Pi,PI,π (hằng số π)" + System.Environment.NewLine;
            help += "             Chú ý: khi muốn sử dụng hằng số π phải gõ π,{pi}, {Pi} hoặc {PI};" + System.Environment.NewLine;
            help += "Cách sử dụng hàm số và một số quy ước tính toán" + System.Environment.NewLine;
            help += "      + Tên hàm: sqrt, sin, cos, tan" + System.Environment.NewLine;
            help += "             Chú ý: Khi sử dụng hàm số, chỉ việc gõ tên hàm, không cần dùng ngoặc nhọn {}, nhưng phải mở ngoặc tròn sau tên hàm, nếu không sẽ bị hiểu là đang dùng biến" + System.Environment.NewLine;
            help += "             Ví dụ: abcsin(pi) = a * b * c * sin(p * i)" + System.Environment.NewLine;
            help += "                    sin{pi} = s * i * n * π" + System.Environment.NewLine;
            help += "      + Các dấu:" + System.Environment.NewLine;
            help += "             - Unary: + - (dương và âm)" + System.Environment.NewLine;
            help += "             - Binary: + - * / ^ (cộng trừ, nhân, chia, mũ)" + System.Environment.NewLine;
            help += "             - =, >, <, >=, <= (các dấu của biểu thức)" + System.Environment.NewLine;
            help += "             - ( )" + System.Environment.NewLine;
            help += "             - . (dấu phẩy động)" + System.Environment.NewLine;
            help += "             - , (dấu phẩy ngăn cách tham số giữa các hàm - tính năng có cài đặt nhưng chưa sử dụng)" + System.Environment.NewLine;
            help += "             - Omited binary operator: dấu nhân bị lược bỏ. Ví dụ ab có nghĩa là a * b" + System.Environment.NewLine;
            help += "                     Chú ý: Đôi khi người dùng nhập x / ab với ý nghĩa là x / (a * b), mặc dù theo như quy ước toán học là đối với hai phép tính toán học * và / có cùng độ ưu tiên thì thực hiện từ trái qua phải, tuy nhiên do tính ngữ nghĩa như trên, nên hệ thống sẽ hiểu phép nhân bị lược bỏ có độ ưu tiên CAO HƠN phép nhân và chia có dấu, do đó với 7 / ab thì phép nhân thực hiện trước, phép chia thực hiện sau." + System.Environment.NewLine;
            help += "             * Ví dụ: f(5 * +-+-+-3) = f(5 * -3) = -15" + System.Environment.NewLine;
            help += "                      f(abc,a = 5,b = 6,c = 3) = f(5 * 6 * 3) = 90" + System.Environment.NewLine;
            help += "                      f(9 / 3(1 + 2)) = f(9 / (3 * 3)) = 1" + System.Environment.NewLine;
            help += "                      f(9 / 3 * (1 + 2)) = f(3 * 3) = 9" + System.Environment.NewLine;
            
            System.Windows.Forms.MessageBox.Show(help, "Help", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string filename;
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            savedialog.FilterIndex = 1;
            savedialog.ShowDialog();
            filename = savedialog.FileName;
            if (filename != "")
            {
                if (FormMode == 0)
                {
                    SaveCollectionData();
                }
                File.WriteAllText(filename, textBoxOutput.Text);
            }
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            string filename;
            OpenFileDialog opendialog = new OpenFileDialog();
            opendialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            opendialog.FilterIndex = 1;
            opendialog.ShowDialog();
            filename = opendialog.FileName;
            if (filename != "")
            {
                textBoxOutput.Text = File.ReadAllText(filename);
                if (FormMode == 0)
                {
                    dataGridExpression.ItemsSource = LoadCollectionData();
                }
            }
        }
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            if (FormType == 0)
            {
                textBoxOutput.Text = MainWindow.defaultexpression;
            }
            else textBoxOutput.Text = "0" + System.Environment.NewLine + "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;
            if (FormMode == 0)
            {
                dataGridExpression.ItemsSource = LoadCollectionData();
            }
        }
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (FormMode == 0)
            {
                SaveCollectionData();
            }
            if (FormType == 0 && textBoxOutput.Text != MainWindow.defaultexpression) original = 0;
            FormExpressions = textBoxOutput.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            Close();
        }
        private List<Expression> loadCollectionData()
        {
            List<Expression> expressions = new List<Expression>();
            Expression expression;
            string[] exceptionReading;
            int nFunction, nException, nAssignment, nExceptionType1, nExceptionType2, nExceptionImpact, i, j, k;
            FormExpressions = textBoxOutput.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            nFunction = Int32.Parse(FormExpressions[0]);
            nException = Int32.Parse(FormExpressions[1]);
            nAssignment = Int32.Parse(FormExpressions[2]);
            for (i = 0; i < nFunction; ++i)
            {
                expression = new Expression();
                expression.Data = FormExpressions[i + 3];
                expressions.Add(expression);
            }
            for (i = 0, j = nFunction + 3; i < nException; ++i, ++j)
            {
                exceptionReading = FormExpressions[j].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                nExceptionType1 = Int32.Parse(exceptionReading[0]);
                nExceptionType2 = Int32.Parse(exceptionReading[1]);
                nExceptionImpact = Int32.Parse(exceptionReading[2]);
                for (k = 0; k < nExceptionImpact; ++k)
                {
                    int l;
                    int kk = Int32.Parse(exceptionReading[3 + nExceptionType1 + nExceptionType2 + k]);
                    for (l = 0; l < nExceptionType1; ++l)
                    {
                        if (l != 0) expressions[kk].ExceptionElements += " ";
                        expressions[kk].ExceptionElements += exceptionReading[l + 3];
                    }
                    for (l = 0; l < nExceptionType2; ++l)
                    {
                        if (l != 0) expressions[kk].ForbiddenElements += " ";
                        expressions[kk].ForbiddenElements += exceptionReading[l + nExceptionType1 + 3];
                    }
                }
            }
            if (nAssignment != 0)
            {
                exceptionReading = FormExpressions[nFunction + nException + 3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                for (i = 0; i < exceptionReading.Length; ++i)
                {
                    expressions[Int32.Parse(exceptionReading[i])].IsAssignment = true;
                }
            }
            return expressions;
        }
        private List<Expression> LoadCollectionData()
        {
            List<Expression> res = null;
            try
            {
                res = loadCollectionData();
            }
            catch (Exception excep)
            {
                System.Windows.MessageBox.Show("Có lỗi xảy ra: " + excep.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                if (FormType == 0)
                {
                    textBoxOutput.Text = MainWindow.defaultexpression;
                }
                else textBoxOutput.Text = "0" + System.Environment.NewLine + "0" + System.Environment.NewLine + "0" + System.Environment.NewLine;
                res = loadCollectionData();
            }
            return res;
        }
        private void SaveCollectionData()
        {
            int nFunction, nException, nAssignment = 0, i, j;
            Expression expression;
            List<string> expressions = new List<string>();
            Dictionary<string,string> exceptions = new Dictionary<string,string>();
            string[] tempBreaker1,tempBreaker2;
            string strAssignment = "";
            string strException,strFunction;
            textBoxOutput.Text = "";
            nFunction = dataGridExpression.Items.Count - 1;
            expressions.Add(nFunction.ToString());
            expressions.Add("0");
            expressions.Add("0");
            for (i = 0;i < nFunction;++i)
            {
                expression = (Expression)dataGridExpression.Items[i];
                expressions.Add(expression.Data);
                if (expression.IsAssignment)
                {
                    if (strAssignment.Length != 0) strAssignment += " ";
                    strAssignment += i.ToString();
                    ++nAssignment;
                }
                strException = "";
                if (expression.ExceptionElements == null)
                {
                    tempBreaker1 = new string[0];
                }
                else
                {
                    tempBreaker1 = expression.ExceptionElements.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    Array.Sort(tempBreaker1);
                }
                if (expression.ForbiddenElements == null)
                {
                    tempBreaker2 = new string[0];
                }
                else
                {
                    tempBreaker2 = expression.ForbiddenElements.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    Array.Sort(tempBreaker2);
                }
                if (tempBreaker1.Length != 0 || tempBreaker2.Length != 0)
                {
                    strException += tempBreaker1.Length.ToString() + " " + tempBreaker2.Length.ToString();
                    for (j = 0; j < tempBreaker1.Length; ++j) strException += " " + tempBreaker1[j];
                    for (j = 0; j < tempBreaker2.Length; ++j) strException += " " + tempBreaker2[j];
                    if (!exceptions.ContainsKey(strException))
                    {
                        exceptions[strException] = i.ToString();
                    }
                    else
                    {
                        strFunction = exceptions[strException];
                        strFunction += " " + i.ToString();
                        exceptions[strException] = strFunction;
                    }
                }
            }
            nException = exceptions.Count;
            expressions[1] = nException.ToString();
            foreach (KeyValuePair<string, string> ele in exceptions)
            {
                tempBreaker1 = ele.Key.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                tempBreaker2 = ele.Value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                strException = "";
                strException += tempBreaker1[0] + " " + tempBreaker1[1] + " " + tempBreaker2.Length.ToString();
                for (i = 2; i < tempBreaker1.Length; ++i)
                {
                    strException += " " + tempBreaker1[i];
                }
                for (i = 0; i < tempBreaker2.Length; ++i)
                {
                    strException += " " + tempBreaker2[i];
                }
                expressions.Add(strException);
            }
            expressions[2] = nAssignment.ToString();
            if (nAssignment > 0) expressions.Add(strAssignment);
            foreach (string str in expressions)
            {
                textBoxOutput.Text += str + System.Environment.NewLine;
            }
        }
    }
    public class Expression
    {
        public bool IsAssignment { get; set; }
        public string Data { get; set; }
        public string ExceptionElements { get; set; }
        public string ForbiddenElements { get; set; }
    }
}
