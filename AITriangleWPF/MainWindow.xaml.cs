﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;

namespace AITriangleWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[] triangleName = new string[9] { "Normal", "Acute", "Equilateral", "Isosceles", "Right", "Isosceles Right", "Obtuse", "Half Equilateral I", "Half Equilateral II" };
        private string[] angleName = new string[3] { "Degrees", "Radians", "Grads" };
        private string[] triangleAngleType = new string[3] { "A", "B", "C" };
        Point[] helpingPoint;
        private double[] helpingR;
        private Rectangle rectLocker;
        private Line lineA;
        private Line lineB;
        private Line lineC;
        private Line linemA, linemB, linemC;
        private Line linehA, linehB, linehC;
        private Line[,] helpinglineh;
        private Line linepA, linepB, linepC;
        private Line lineRA, lineRB, lineRC;
        private Line linerA, linerB, linerC;
        private Ellipse ellipser, ellipseR;
        private Polygon trianglecanvas;
        private TextBlock textAngleA, textAngleB, textAngleC;
        private TextBlock textSideA, textSideB, textSideC;
        private TextBlock textmA, textmB, textmC;
        private TextBlock texthA, texthB, texthC;
        private TextBlock textpA, textpB, textpC;
        private TextBlock textP, textS, textOutsideCircleR, textInsideCircleR;
        private TextBlock textError;

        private int initialized = 0;
        private int original = 1;
        private string[] expressionSettings;
        private string[] expressionProfiles = new string[3]{"0","0","0"};
        private int[] angleexceptiontype = new int[3]{0,0,0};
        private int angleMode = 0;
        private int triangleType = 0; //0 - Normal; 1 - Acute (Nhọn); 2 - Equilateral (Đều); 3 - Isosceles (Cân); 4 - Right (Vuông); 5 - Isosceles Right (Vuông Cân); 6 - Obtuse (Tù); 7/8 - Half Equilateral (Nửa tam giác đều);-1 - Custom Triangle
        private int triangleTypeAngle = 0;//0 - A; 1 - B; 2 - C
        private int inputTriangleType = 0, inputTriangleTypeAngle = 0;
        private double[] elements = new double[19];
        private int[] isInputElement;
        private int[] isOutputElement;
        private int[] isErrorElement = new int[19];
        private string[] inputstring = new string[19];
        private int highlightElementCanvas = -1;
        private int highlightElementCanvasPrevious = -1;
        private int calculated = 0;
        private string logsOfThinking = "";
        public static string defaultexpression = "32\r\n7\r\n0\r\nA + B + C = {pi}\r\n2p = a + b + c\r\na^2 = b^2 + c^2 - 2bccos(A)\r\nb^2 = c^2 + a^2 - 2cacos(B)\r\nc^2 = a^2 + b^2 - 2abcos(C)\r\na / sin(A) = b / sin(B)\r\nb / sin(B) = c / sin(C)\r\nc / sin(C) = a / sin(A)\r\na / sin(A) = 2R\r\nb / sin(B) = 2R\r\nc / sin(C) = 2R\r\nS = a{hA} / 2\r\nS = b{hB} / 2\r\nS = c{hC} / 2\r\nS = pr\r\nS = sqrt(p(p-a)(p-b)(p-c))\r\nS = bcsin(A)/2\r\nS = casin(B)/2\r\nS = absin(C)/2\r\n{hA} = bsin(C)\r\n{hA} = csin(B)\r\n{hB} = csin(A)\r\n{hB} = asin(C)\r\n{hC} = asin(B)\r\n{hC} = bsin(A)\r\n4{mA}^2 = 2b^2 + 2 c^2 - a^2\r\n4{mB}^2 = 2c^2 + 2 a^2 - b^2\r\n4{mC}^2 = 2a^2 + 2 b^2 - c^2\r\n{pA} = (2 / (b + c))sqrt(bcp(p-a))\r\n{pB} = (2 / (c + a))sqrt(cap(p-b))\r\n{pC} = (2 / (a + b))sqrt(abp(p-c))\r\nR = abc / (4S)\r\n2 0 1 A B 5\r\n2 0 1 B C 6\r\n2 0 1 A C 7\r\n1 0 4 A 8 16 21 24\r\n1 0 4 B 9 17 20 23\r\n1 0 4 C 10 18 19 22\r\n0 4 4 a b c p 15 28 29 30\r\n";
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            string tmp;
            Assembly _assembly;
            StreamReader _textStreamReader;

            try
            {
                _assembly = Assembly.GetExecutingAssembly();
                _textStreamReader = new StreamReader(_assembly.GetManifestResourceStream("AITriangleWPF.expressions.txt"));
                if (_textStreamReader.Peek() != -1)
                {
                    defaultexpression = _textStreamReader.ReadToEnd();
                }
            }
            catch
            {
                MessageBox.Show("Error accessing resources! Using old-database instead!!!!","Warning",MessageBoxButton.OK,MessageBoxImage.Warning);
            }

            initialized = 1;
            tmp = defaultexpression;
            expressionSettings = tmp.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            original = 1;
            helpingPoint = new Point[17];
            for (int i = 0; i < helpingPoint.Length; ++i) helpingPoint[i] = new Point();
            helpinglineh = new Line[3, 2];
            helpingR = new double[2];
            canvasInit();
            canvasDraw(2);
            highlightElementCanvasPrevious = 0;
            PaintingBeforeCalculate(rtbSideA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbSideB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbSideC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbP); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbS); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbOutsideCircleR); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbInsideCircleR); ++highlightElementCanvasPrevious;
            highlightElementCanvasPrevious = -1;
            ColumnTriangleTypeAngle.Width = (GridLength)(new GridLengthConverter()).ConvertFromString("0");
        }
        private void Window_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            canvasDraw(0);
        }
        private void btnAngleType_Click(object sender, RoutedEventArgs e)
        {
            angleMode = (angleMode + 1) % 3;
            btnAngleType.Content = angleName[angleMode];
            if (calculated == 1 || calculated == 2)
            {
                if (isOutputElement[3] == 1) rtbAngleA.Text = formatConvertAngleType(elements[3], 1, angleMode);
                if (isOutputElement[4] == 1) rtbAngleB.Text = formatConvertAngleType(elements[4], 1, angleMode);
                if (isOutputElement[5] == 1) rtbAngleC.Text = formatConvertAngleType(elements[5], 1, angleMode);
            }
            else if (calculated == 0)
            {
                Parser expressionparser = new Parser();
                if (rtbAngleA.Text != "") rtbAngleA.Text = formatConvertAngleType(convertAngleType(expressionparser.quickGetResult(rtbAngleA.Text), (angleMode + 2) % 3, 1), 1, angleMode);
                if (rtbAngleB.Text != "") rtbAngleB.Text = formatConvertAngleType(convertAngleType(expressionparser.quickGetResult(rtbAngleB.Text), (angleMode + 2) % 3, 1), 1, angleMode);
                if (rtbAngleC.Text != "") rtbAngleC.Text = formatConvertAngleType(convertAngleType(expressionparser.quickGetResult(rtbAngleC.Text), (angleMode + 2) % 3, 1), 1, angleMode);
            }
        }
        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            ProfilesandSettingsForm form = new ProfilesandSettingsForm(0,expressionSettings);
            form.ShowDialog();
            expressionSettings = form.getExpressions();
            if (form.original == 0)
            {
                original = 0;
                triangleType = -1;
                btnTriangleType.Content = "Custom";
                ColumnTriangleTypeAngle.Width = (GridLength)(new GridLengthConverter()).ConvertFromString("0");
                System.Windows.Media.LinearGradientBrush BackgroundPainter = new LinearGradientBrush();
                System.Windows.Media.SolidColorBrush BorderPainter = new SolidColorBrush();
                BackgroundPainter.StartPoint = new Point(0.5, 0);
                BackgroundPainter.EndPoint = new Point(0.5, 1);
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.LightGray, 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.LightGray, 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.Gray, 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.Gray, 1.0));
                BorderPainter.Color = Colors.Gray;
                btnTriangleType.Background = BackgroundPainter;
                btnTriangleType.BorderBrush = BorderPainter;
            }
            else
            {
                triangleType = 0;
                btnTriangleType.Content = triangleName[triangleType];
                GridLengthConverter myGridLengthConverter = new GridLengthConverter();
                if (triangleType == 0 || triangleType == 1 || triangleType == 2)
                {
                    ColumnTriangleTypeAngle.Width = (GridLength)myGridLengthConverter.ConvertFromString("0");
                }
                else
                {
                    ColumnTriangleTypeAngle.Width = (GridLength)myGridLengthConverter.ConvertFromString("28");
                }
                btnTriangleType.Foreground = btnTriangleTypeAngle.Foreground;
                btnTriangleType.Background = btnTriangleTypeAngle.Background;
                btnTriangleType.IsEnabled = true;
            }
            setupAfterTriangleSetting();
        }
        private void btnProfile_Click(object sender, RoutedEventArgs e)
        {
            ProfilesandSettingsForm form = new ProfilesandSettingsForm(1,expressionProfiles);
            form.ShowDialog();
            expressionProfiles = form.getExpressions();
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            int savecalculated = calculated;
            calculated = 0;
            highlightElementCanvas = -1;
            highlightElementCanvasPrevious = 0;
            PaintingBeforeCalculate(rtbSideA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbSideB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbSideC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbP); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbS); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbOutsideCircleR); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbInsideCircleR); ++highlightElementCanvasPrevious;
            PaintingBoxBeforeCalculate(btnTriangleType);
            richTextBoxOutput.Text = "";
            logsOfThinking = "";
            highlightElementCanvasPrevious = -1;
            if (original == 1)
            {
                triangleType = 0;
                triangleTypeAngle = 0;
                btnTriangleType.Content = triangleName[0];
                btnTriangleTypeAngle.Content = "A";
                ColumnTriangleTypeAngle.Width = (GridLength)(new GridLengthConverter()).ConvertFromString("0");
            }
            angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 0;
            btnCalculate.IsEnabled = true;
            if (savecalculated == 1 || savecalculated == 2)
            {
                triangleType = (inputTriangleType + triangleName.Length - 1) % triangleName.Length;
                triangleTypeAngle = (inputTriangleTypeAngle + triangleAngleType.Length - 1) % triangleAngleType.Length;
                btnTriangleType_Click(btnTriangleType,null);
                btnTriangleTypeAngle_Click(btnTriangleTypeAngle,null);
                int i;
                i = 0; rtbSideA.Text = inputstring[i];
                i = 1; rtbSideB.Text = inputstring[i];
                i = 2; rtbSideC.Text = inputstring[i];
                i = 3; rtbAngleA.Text = inputstring[i];
                i = 4; rtbAngleB.Text = inputstring[i];
                i = 5; rtbAngleC.Text = inputstring[i];
                i = 6; rtbmA.Text = inputstring[i];
                i = 7; rtbmB.Text = inputstring[i];
                i = 8; rtbmC.Text = inputstring[i];
                i = 9; rtbhA.Text = inputstring[i];
                i = 10; rtbhB.Text = inputstring[i];
                i = 11; rtbhC.Text = inputstring[i];
                i = 12; rtbpA.Text = inputstring[i];
                i = 13; rtbpB.Text = inputstring[i];
                i = 14; rtbpC.Text = inputstring[i];
                i = 15; rtbP.Text = inputstring[i];
                i = 16; rtbS.Text = inputstring[i];
                i = 17; rtbOutsideCircleR.Text = inputstring[i];
                i = 18; rtbInsideCircleR.Text = inputstring[i];
            }

            canvasDraw(2);
        }
        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Parser expressionparser = new Parser();
                int i;
                string str;
                isInputElement = new int[elements.Length];
                for (i = 0; i < isInputElement.Length; ++i) isInputElement[i] = 0;
                i = 0; str = rtbSideA.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 1; str = rtbSideB.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 2; str = rtbSideC.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 3; str = rtbAngleA.Text; if (str != "") { isInputElement[i] = 1; elements[i] = convertAngleType(expressionparser.quickGetResult(str), angleMode, 1); } inputstring[i] = str;
                i = 4; str = rtbAngleB.Text; if (str != "") { isInputElement[i] = 1; elements[i] = convertAngleType(expressionparser.quickGetResult(str), angleMode, 1); } inputstring[i] = str;
                i = 5; str = rtbAngleC.Text; if (str != "") { isInputElement[i] = 1; elements[i] = convertAngleType(expressionparser.quickGetResult(str), angleMode, 1); } inputstring[i] = str;
                i = 6; str = rtbmA.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 7; str = rtbmB.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 8; str = rtbmC.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 9; str = rtbhA.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 10; str = rtbhB.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 11; str = rtbhC.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 12; str = rtbpA.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 13; str = rtbpB.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 14; str = rtbpC.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 15; str = rtbP.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 16; str = rtbS.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 17; str = rtbOutsideCircleR.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                i = 18; str = rtbInsideCircleR.Text; if (str != "") { isInputElement[i] = 1; elements[i] = expressionparser.quickGetResult(str); } inputstring[i] = str;
                isOutputElement = new int[elements.Length];
                inputTriangleType = triangleType;
                inputTriangleTypeAngle = triangleTypeAngle;
                calculation();
                richTextBoxOutput.Text = logsOfThinking;
                for (i = 0; i < 3; ++i) elements[i] = Sensitive(elements[i]);
                for (i = 6; i < elements.Length; ++i) elements[i] = Sensitive(elements[i]);
                highlightElementCanvas = -1;
                highlightElementCanvasPrevious = 0;
                PaintingAfterCalculate(rtbSideA); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbSideB); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbSideC); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbAngleA); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbAngleB); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbAngleC); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbmA); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbmB); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbmC); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbhA); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbhB); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbhC); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbpA); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbpB); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbpC); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbP); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbS); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbOutsideCircleR); ++highlightElementCanvasPrevious;
                PaintingAfterCalculate(rtbInsideCircleR); ++highlightElementCanvasPrevious;
                PaintingBoxAfterCalculate(btnTriangleType);
                highlightElementCanvasPrevious = -1;
                canvasDraw(2);
                btnCalculate.IsEnabled = false;
            }
            catch (Exception excep)
            {
                MessageBox.Show("Có lỗi xảy ra: " + excep.Message + "\r\nTại " + excep.StackTrace.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void canvasInit()
        {
            int i, j;
            SolidColorBrush HelpingLineBrush = new SolidColorBrush();
            SolidColorBrush HighlightedBrush = new SolidColorBrush();
            SolidColorBrush InvisibleBrush = new SolidColorBrush();
            DoubleCollection dashes = new DoubleCollection();
            HelpingLineBrush.Color = Color.FromArgb(255, 126, 126, 126);
            HighlightedBrush.Color = Color.FromArgb(255, 55, 130, 205);
            InvisibleBrush.Color = Color.FromArgb(0, 0, 0, 0);
            dashes.Add(3);
            dashes.Add(3);
            rectLocker = new Rectangle();
            lineA = new Line();
            lineB = new Line();
            lineC = new Line();
            linemA = new Line();
            linemB = new Line();
            linemC = new Line();
            linehA = new Line();
            linehB = new Line();
            linehC = new Line();
            linepA = new Line();
            linepB = new Line();
            linepC = new Line();
            lineRA = new Line();
            lineRB = new Line();
            lineRC = new Line();
            linerA = new Line();
            linerB = new Line();
            linerC = new Line();
            ellipser = new Ellipse();
            ellipseR = new Ellipse();
            trianglecanvas = new Polygon();
            for (i = 0; i < 3; ++i)
                for (j = 0; j < 2; ++j)
                {
                    helpinglineh[i, j] = new Line();
                    helpinglineh[i, j].Stroke = HelpingLineBrush;
                    helpinglineh[i, j].StrokeThickness = 1;
                    helpinglineh[i, j].StrokeDashArray = dashes;
                }
            textAngleA = new TextBlock();
            textAngleB = new TextBlock();
            textAngleC = new TextBlock();
            textSideA = new TextBlock();
            textSideB = new TextBlock();
            textSideC = new TextBlock();
            textmA = new TextBlock();
            textmB = new TextBlock();
            textmC = new TextBlock();
            texthA = new TextBlock();
            texthB = new TextBlock();
            texthC = new TextBlock();
            textpA = new TextBlock();
            textpB = new TextBlock();
            textpC = new TextBlock();
            textP = new TextBlock();
            textS = new TextBlock();
            textOutsideCircleR = new TextBlock();
            textInsideCircleR = new TextBlock();
            textError = new TextBlock();
            linemA.Stroke = linemB.Stroke = linemC.Stroke = HighlightedBrush;
            linehA.Stroke = linehB.Stroke = linehC.Stroke = HighlightedBrush;
            linepA.Stroke = linepB.Stroke = linepC.Stroke = HighlightedBrush;
            lineRA.Stroke = lineRB.Stroke = lineRC.Stroke = HighlightedBrush;
            linerA.Stroke = linerB.Stroke = linerC.Stroke = HighlightedBrush;
            textSideA.Foreground = textSideB.Foreground = textSideC.Foreground = HelpingLineBrush;
            textAngleA.Foreground = textAngleB.Foreground = textAngleC.Foreground = HelpingLineBrush;
            textmA.Foreground = textmB.Foreground = textmC.Foreground = InvisibleBrush;
            texthA.Foreground = texthB.Foreground = texthC.Foreground = InvisibleBrush;
            textpA.Foreground = textpB.Foreground = textpC.Foreground = InvisibleBrush;
            textP.Foreground = textS.Foreground = textOutsideCircleR.Foreground = textInsideCircleR.Foreground = InvisibleBrush;
            textmA.FontWeight = textmB.FontWeight = textmC.FontWeight = FontWeights.Bold;
            texthA.FontWeight = texthB.FontWeight = texthC.FontWeight = FontWeights.Bold;
            textpA.FontWeight = textpB.FontWeight = textpC.FontWeight = FontWeights.Bold;
            textP.FontWeight = textS.FontWeight = textOutsideCircleR.FontWeight = textInsideCircleR.FontWeight = FontWeights.Bold;
            ellipser.StrokeDashArray = ellipseR.StrokeDashArray = dashes;
            ellipser.Stroke = ellipseR.Stroke = HelpingLineBrush;
            textAngleA.Text = "A";
            textAngleB.Text = "B";
            textAngleC.Text = "C";
            textSideA.Text = "a";
            textSideB.Text = "b";
            textSideC.Text = "c";
            textmA.Text = "mA";
            textmB.Text = "mB";
            textmC.Text = "mC";
            texthA.Text = "hA";
            texthB.Text = "hB";
            texthC.Text = "hC";
            textpA.Text = "pA";
            textpB.Text = "pB";
            textpC.Text = "pC";
            textP.Text = "p";
            textS.Text = "S";
            textOutsideCircleR.Text = "R";
            textInsideCircleR.Text = "r";
            textError.Text = "Không thể giải được tam giác vì\r\ncác yếu tố không đầy đủ hoặc không phù hợp";
            textError.TextWrapping = TextWrapping.Wrap;
            textError.FontWeight = FontWeights.Bold;
            textError.TextAlignment = TextAlignment.Center;
            canvasTriangle.Children.Add(lineA);
            canvasTriangle.Children.Add(lineB);
            canvasTriangle.Children.Add(lineC);
            canvasTriangle.Children.Add(linemA);
            canvasTriangle.Children.Add(linemB);
            canvasTriangle.Children.Add(linemC);
            canvasTriangle.Children.Add(linehA);
            canvasTriangle.Children.Add(linehB);
            canvasTriangle.Children.Add(linehC);
            canvasTriangle.Children.Add(linepA);
            canvasTriangle.Children.Add(linepB);
            canvasTriangle.Children.Add(linepC);
            canvasTriangle.Children.Add(lineRA);
            canvasTriangle.Children.Add(lineRB);
            canvasTriangle.Children.Add(lineRC);
            canvasTriangle.Children.Add(linerA);
            canvasTriangle.Children.Add(linerB);
            canvasTriangle.Children.Add(linerC);
            canvasTriangle.Children.Add(ellipser);
            canvasTriangle.Children.Add(ellipseR);
            canvasTriangle.Children.Add(trianglecanvas);
            canvasTriangle.Children.Add(textAngleA);
            canvasTriangle.Children.Add(textAngleB);
            canvasTriangle.Children.Add(textAngleC);
            canvasTriangle.Children.Add(textSideA);
            canvasTriangle.Children.Add(textSideB);
            canvasTriangle.Children.Add(textSideC);
            canvasTriangle.Children.Add(textmA);
            canvasTriangle.Children.Add(textmB);
            canvasTriangle.Children.Add(textmC);
            canvasTriangle.Children.Add(texthA);
            canvasTriangle.Children.Add(texthB);
            canvasTriangle.Children.Add(texthC);
            canvasTriangle.Children.Add(textpA);
            canvasTriangle.Children.Add(textpB);
            canvasTriangle.Children.Add(textpC);
            canvasTriangle.Children.Add(textP);
            canvasTriangle.Children.Add(textS);
            canvasTriangle.Children.Add(textOutsideCircleR);
            canvasTriangle.Children.Add(textInsideCircleR);
            canvasTriangle.Children.Add(rectLocker);
            canvasTriangle.Children.Add(textError);
            for (i = 0; i < 3; ++i)
                for (j = 0; j < 2; ++j)
                    canvasTriangle.Children.Add(helpinglineh[i, j]);
        }
        private void canvasDraw(int update)
        {
            try
            {
                if (initialized == 0) return;
                SolidColorBrush HighlightedBrush, RestoreBrush, RectangleLockerBrush, TriangleHighlightedBrush, InvisibleBrush, TextErrorBrush;
                int i, j;
                double A = 0, B = 0, C = 0;
                double baseX = 0, baseY = 0, zoomFactor = 30;
                double vectorAx, vectorBx, vectorCx, vectorAy, vectorBy, vectorCy, D, Dx, Dy;
                baseX = 200;
                baseY = 50;
                zoomFactor = 30;
                double xmax, xmin, ymax, ymin, x2max, x2min, y2max, y2min;
                if (update == 0 || update == 2)
                {
                    HighlightedBrush = new SolidColorBrush();
                    RestoreBrush = new SolidColorBrush();
                    TriangleHighlightedBrush = new SolidColorBrush();
                    InvisibleBrush = new SolidColorBrush();
                    HighlightedBrush.Color = Color.FromArgb(255, 55, 130, 205);
                    RestoreBrush.Color = Color.FromArgb(255, 126, 126, 126);
                    TriangleHighlightedBrush.Color = Color.FromArgb(48, 128, 128, 128);
                    InvisibleBrush.Color = Color.FromArgb(0, 0, 0, 0);
                    if (highlightElementCanvas != highlightElementCanvasPrevious)
                    {
                        switch (highlightElementCanvas)
                        {
                            case 0: lineA.Stroke = HighlightedBrush; lineA.StrokeThickness = 1.5; textSideA.Foreground = HighlightedBrush; textSideA.FontWeight = FontWeights.Bold; break;
                            case 1: lineB.Stroke = HighlightedBrush; lineB.StrokeThickness = 1.5; textSideB.Foreground = HighlightedBrush; textSideB.FontWeight = FontWeights.Bold; break;
                            case 2: lineC.Stroke = HighlightedBrush; lineC.StrokeThickness = 1.5; textSideC.Foreground = HighlightedBrush; textSideC.FontWeight = FontWeights.Bold; break;
                            case 3: textAngleA.Foreground = HighlightedBrush; textAngleA.FontWeight = FontWeights.Bold; break;
                            case 4: textAngleB.Foreground = HighlightedBrush; textAngleB.FontWeight = FontWeights.Bold; break;
                            case 5: textAngleC.Foreground = HighlightedBrush; textAngleC.FontWeight = FontWeights.Bold; break;
                            case 6: linemA.StrokeThickness = 1.5; textmA.Foreground = HighlightedBrush; break;
                            case 7: linemB.StrokeThickness = 1.5; textmB.Foreground = HighlightedBrush; break;
                            case 8: linemC.StrokeThickness = 1.5; textmC.Foreground = HighlightedBrush; break;
                            case 9: linehA.StrokeThickness = 1.5; helpinglineh[0, 0].StrokeThickness = 1; helpinglineh[0, 1].StrokeThickness = 1; texthA.Foreground = HighlightedBrush; break;
                            case 10: linehB.StrokeThickness = 1.5; helpinglineh[1, 0].StrokeThickness = 1; helpinglineh[1, 1].StrokeThickness = 1; texthB.Foreground = HighlightedBrush; break;
                            case 11: linehC.StrokeThickness = 1.5; helpinglineh[2, 0].StrokeThickness = 1; helpinglineh[2, 1].StrokeThickness = 1; texthC.Foreground = HighlightedBrush; break;
                            case 12: linepA.StrokeThickness = 1.5; textpA.Foreground = HighlightedBrush; break;
                            case 13: linepB.StrokeThickness = 1.5; textpB.Foreground = HighlightedBrush; break;
                            case 14: linepC.StrokeThickness = 1.5; textpC.Foreground = HighlightedBrush; break;
                            case 15: lineA.Stroke = lineB.Stroke = lineC.Stroke = HighlightedBrush; lineA.StrokeThickness = lineB.StrokeThickness = lineC.StrokeThickness = 1.5; textP.Foreground = HighlightedBrush; break;
                            case 16: trianglecanvas.Fill = TriangleHighlightedBrush; textS.Foreground = HighlightedBrush; break;
                            case 17: lineRA.StrokeThickness = lineRB.StrokeThickness = lineRC.StrokeThickness = 1.5; ellipseR.StrokeThickness = 1; textOutsideCircleR.Foreground = HighlightedBrush; break;
                            case 18: linerA.StrokeThickness = linerB.StrokeThickness = linerC.StrokeThickness = 1.5; ellipser.StrokeThickness = 1; textInsideCircleR.Foreground = HighlightedBrush; break;
                        }
                    }
                    switch (highlightElementCanvasPrevious)
                    {
                        case 0: if (highlightElementCanvas == 15) break; lineA.Stroke = RestoreBrush; lineA.StrokeThickness = 1; textSideA.Foreground = RestoreBrush; textSideA.FontWeight = FontWeights.Normal; break;
                        case 1: if (highlightElementCanvas == 15) break; lineB.Stroke = RestoreBrush; lineB.StrokeThickness = 1; textSideB.Foreground = RestoreBrush; textSideB.FontWeight = FontWeights.Normal; break;
                        case 2: if (highlightElementCanvas == 15) break; lineC.Stroke = RestoreBrush; lineC.StrokeThickness = 1; textSideC.Foreground = RestoreBrush; textSideC.FontWeight = FontWeights.Normal; break;
                        case 3: textAngleA.Foreground = RestoreBrush; textAngleA.FontWeight = FontWeights.Normal; break;
                        case 4: textAngleB.Foreground = RestoreBrush; textAngleB.FontWeight = FontWeights.Normal; break;
                        case 5: textAngleC.Foreground = RestoreBrush; textAngleC.FontWeight = FontWeights.Normal; break;
                        case 6: linemA.StrokeThickness = 0; textmA.Foreground = InvisibleBrush; break;
                        case 7: linemB.StrokeThickness = 0; textmB.Foreground = InvisibleBrush; break;
                        case 8: linemC.StrokeThickness = 0; textmC.Foreground = InvisibleBrush; break;
                        case 9: linehA.StrokeThickness = 0; helpinglineh[0, 0].StrokeThickness = 0; helpinglineh[0, 1].StrokeThickness = 0; texthA.Foreground = InvisibleBrush; break;
                        case 10: linehB.StrokeThickness = 0; helpinglineh[1, 0].StrokeThickness = 0; helpinglineh[1, 1].StrokeThickness = 0; texthB.Foreground = InvisibleBrush; break;
                        case 11: linehC.StrokeThickness = 0; helpinglineh[2, 0].StrokeThickness = 0; helpinglineh[2, 1].StrokeThickness = 0; texthC.Foreground = InvisibleBrush; break;
                        case 12: linepA.StrokeThickness = 0; textpA.Foreground = InvisibleBrush; break;
                        case 13: linepB.StrokeThickness = 0; textpB.Foreground = InvisibleBrush; break;
                        case 14: linepC.StrokeThickness = 0; textpC.Foreground = InvisibleBrush; break;
                        case 15: if (highlightElementCanvas == 0 || highlightElementCanvas == 1 || highlightElementCanvas == 2) break; lineA.Stroke = lineB.Stroke = lineC.Stroke = RestoreBrush; lineA.StrokeThickness = lineB.StrokeThickness = lineC.StrokeThickness = 1; textP.Foreground = InvisibleBrush; break;
                        case 16: trianglecanvas.Fill = null; textS.Foreground = InvisibleBrush; break;
                        case 17: lineRA.StrokeThickness = lineRB.StrokeThickness = lineRC.StrokeThickness = ellipseR.StrokeThickness = 0; textOutsideCircleR.Foreground = InvisibleBrush; break;
                        case 18: linerA.StrokeThickness = linerB.StrokeThickness = linerC.StrokeThickness = ellipser.StrokeThickness = 0; textInsideCircleR.Foreground = InvisibleBrush; break;
                    }
                }
                if (update == 1 || update == 2)
                {
                    if (calculated == 0)
                    {
                        switch (triangleType)
                        {
                            case -1:
                            case 0:
                            case 1:
                                B = Math.Atan(2);
                                C = Math.Atan(4.0 / 3);
                                A = Math.PI - B - C;
                                break;
                            case 2:
                                A = B = C = Math.PI / 3;
                                break;
                            case 3:
                                A = B = C = Math.Atan(4.0 / 3);
                                switch (triangleTypeAngle)
                                {
                                    case 0: A = Math.PI - B - C; break;
                                    case 1: B = Math.PI - A - C; break;
                                    case 2: C = Math.PI - A - B; break;
                                }
                                break;
                            case 4:
                                switch (triangleTypeAngle)
                                {
                                    case 0: A = Math.PI / 2; B = Math.Atan(4.0 / 3); C = Math.Atan(3.0 / 4); break;
                                    case 1: B = Math.PI / 2; C = Math.Atan(4.0 / 3); A = Math.Atan(3.0 / 4); break;
                                    case 2: C = Math.PI / 2; A = Math.Atan(4.0 / 3); B = Math.Atan(3.0 / 4); break;
                                }
                                break;
                            case 5:
                                A = B = C = Math.PI / 4;
                                switch (triangleTypeAngle)
                                {
                                    case 0: A = Math.PI / 2; break;
                                    case 1: B = Math.PI / 2; break;
                                    case 2: C = Math.PI / 2; break;
                                }
                                break;
                            case 6:
                                switch (triangleTypeAngle)
                                {
                                    case 0: A = convertAngleType(120, 0, 1); B = convertAngleType(40, 0, 1); C = convertAngleType(20, 0, 1); break;
                                    case 1: B = convertAngleType(120, 0, 1); C = convertAngleType(40, 0, 1); A = convertAngleType(20, 0, 1); break;
                                    case 2: C = convertAngleType(120, 0, 1); A = convertAngleType(40, 0, 1); B = convertAngleType(20, 0, 1); break;
                                }
                                break;
                            case 7:
                                switch (triangleTypeAngle)
                                {
                                    case 0: A = Math.PI / 2; B = Math.PI / 3; C = Math.PI / 6; break;
                                    case 1: A = Math.PI / 6; B = Math.PI / 2; C = Math.PI / 3; break;
                                    case 2: A = Math.PI / 3; B = Math.PI / 6; C = Math.PI / 2; break;
                                }
                                break;
                            case 8:
                                switch (triangleTypeAngle)
                                {
                                    case 0: A = Math.PI / 2; B = Math.PI / 6; C = Math.PI / 3; break;
                                    case 1: A = Math.PI / 3; B = Math.PI / 2; C = Math.PI / 6; break;
                                    case 2: A = Math.PI / 6; B = Math.PI / 3; C = Math.PI / 2; break;
                                }
                                break;
                            default: throw new Exception("Lỗi vẽ tam giác: ko tìm thấy loại tam giác");
                        }
                    }
                    else if (calculated == 1)
                    {
                        A = elements[3];
                        B = elements[4];
                        C = elements[5];
                    }
                    else if (calculated == 2)
                    {
                        B = Math.Atan(2);
                        C = Math.Atan(4.0 / 3);
                        A = Math.PI - B - C;
                    }
                    //Calculate helpingPoint: HelpingPoint help us to draw the triangle base on the angles.
                    //Point A is always on the top at 0 0, BC is at the bottom, AHa = 4 => B.Y and C.Y = 4, Ha.X = 0, Ha.Y = 4
                    helpingPoint[0].X = helpingPoint[0].Y = 0;
                    helpingPoint[1].Y = helpingPoint[2].Y = 4;
                    if (B == Math.PI / 2) helpingPoint[1].X = 0; else helpingPoint[1].X = -4 / Math.Tan(B);
                    if (C == Math.PI / 2) helpingPoint[2].X = 0; else helpingPoint[2].X = 4 / Math.Tan(C);
                    helpingPoint[3].X = (helpingPoint[1].X + helpingPoint[2].X) / 2; //mA => mid BC
                    helpingPoint[3].Y = (helpingPoint[1].Y + helpingPoint[2].Y) / 2;
                    helpingPoint[4].X = (helpingPoint[2].X + helpingPoint[0].X) / 2; //mB => mid AC
                    helpingPoint[4].Y = (helpingPoint[2].Y + helpingPoint[0].Y) / 2;
                    helpingPoint[5].X = (helpingPoint[0].X + helpingPoint[1].X) / 2; //mC => mid AB
                    helpingPoint[5].Y = (helpingPoint[0].Y + helpingPoint[1].Y) / 2;
                    helpingPoint[6].X = 0; helpingPoint[6].Y = 4;
                    vectorAx = helpingPoint[1].X - helpingPoint[2].X;
                    vectorAy = helpingPoint[1].Y - helpingPoint[2].Y;
                    vectorBx = helpingPoint[2].X - helpingPoint[0].X;
                    vectorBy = helpingPoint[2].Y - helpingPoint[0].Y;
                    vectorCx = helpingPoint[0].X - helpingPoint[1].X;
                    vectorCy = helpingPoint[0].Y - helpingPoint[1].Y;
                    D = -vectorBx * vectorBx - vectorBy * vectorBy;
                    Dx = -(vectorBx * helpingPoint[1].X + vectorBy * helpingPoint[1].Y) * vectorBx + vectorBy * (vectorBx * helpingPoint[0].Y + vectorBy * helpingPoint[0].X);
                    Dy = -vectorBx * (vectorBx * helpingPoint[0].Y + helpingPoint[0].X * vectorBy) - vectorBy * (vectorBx * helpingPoint[1].X + vectorBy * helpingPoint[1].Y);
                    helpingPoint[7].X = Dx / D; helpingPoint[7].Y = Dy / D;
                    D = -vectorCx * vectorCx - vectorCy * vectorCy;
                    Dx = -(vectorCx * helpingPoint[2].X + vectorCy * helpingPoint[2].Y) * vectorCx + vectorCy * (vectorCx * helpingPoint[0].Y + vectorCy * helpingPoint[0].X);
                    Dy = -vectorCx * (vectorCx * helpingPoint[0].Y + helpingPoint[0].X * vectorCy) - vectorCy * (vectorCx * helpingPoint[2].X + vectorCy * helpingPoint[2].Y);
                    helpingPoint[8].X = Dx / D; helpingPoint[8].Y = Dy / D;
                    D = distance(helpingPoint[0], helpingPoint[1]) / (distance(helpingPoint[0], helpingPoint[1]) + distance(helpingPoint[0], helpingPoint[2]));
                    helpingPoint[9].X = helpingPoint[1].X + D * (helpingPoint[2].X - helpingPoint[1].X);
                    helpingPoint[9].Y = helpingPoint[1].Y + D * (helpingPoint[2].Y - helpingPoint[1].Y);
                    D = distance(helpingPoint[1], helpingPoint[2]) / (distance(helpingPoint[1], helpingPoint[2]) + distance(helpingPoint[1], helpingPoint[0]));
                    helpingPoint[10].X = helpingPoint[2].X + D * (helpingPoint[0].X - helpingPoint[2].X);
                    helpingPoint[10].Y = helpingPoint[2].Y + D * (helpingPoint[0].Y - helpingPoint[2].Y);
                    D = distance(helpingPoint[2], helpingPoint[0]) / (distance(helpingPoint[2], helpingPoint[0]) + distance(helpingPoint[2], helpingPoint[1]));
                    helpingPoint[11].X = helpingPoint[0].X + D * (helpingPoint[1].X - helpingPoint[0].X);
                    helpingPoint[11].Y = helpingPoint[0].Y + D * (helpingPoint[1].Y - helpingPoint[0].Y);
                    vectorBx = helpingPoint[1].X - helpingPoint[0].X;
                    vectorBy = helpingPoint[1].Y - helpingPoint[0].Y;
                    vectorCx = helpingPoint[2].X - helpingPoint[0].X;
                    vectorCy = helpingPoint[2].Y - helpingPoint[0].Y;
                    D = 2 * (vectorBx * vectorCy - vectorBy * vectorCx);
                    Dx = (vectorCy * (vectorBx * vectorBx + vectorBy * vectorBy) - vectorBy * (vectorCx * vectorCx + vectorCy * vectorCy)) / D;
                    Dy = (vectorBx * (vectorCx * vectorCx + vectorCy * vectorCy) - vectorCx * (vectorBx * vectorBx + vectorBy * vectorBy)) / D;
                    helpingPoint[12].X = Dx + helpingPoint[0].X;
                    helpingPoint[12].Y = Dy + helpingPoint[0].Y;
                    helpingR[0] = distance(helpingPoint[12], helpingPoint[0]) * 2;
                    vectorAx = (helpingPoint[2].Y - helpingPoint[11].Y) / (helpingPoint[2].X - helpingPoint[11].X);
                    vectorAy = helpingPoint[2].Y - vectorAx * helpingPoint[2].X;
                    vectorBx = (helpingPoint[1].Y - helpingPoint[10].Y) / (helpingPoint[1].X - helpingPoint[10].X);
                    vectorBy = helpingPoint[1].Y - vectorBx * helpingPoint[1].X;
                    helpingPoint[13].X = -(vectorAy - vectorBy) / (vectorAx - vectorBx);
                    helpingPoint[13].Y = vectorAx * helpingPoint[13].X + vectorAy;
                    helpingPoint[14].X = helpingPoint[13].X;
                    helpingPoint[14].Y = 4;
                    D = Math.Cos(A / 2) * distance(helpingPoint[0], helpingPoint[13]) / distance(helpingPoint[0], helpingPoint[1]);
                    helpingPoint[15].X = helpingPoint[1].X * D;
                    helpingPoint[15].Y = helpingPoint[1].Y * D;
                    D = Math.Cos(A / 2) * distance(helpingPoint[0], helpingPoint[13]) / distance(helpingPoint[0], helpingPoint[2]);
                    helpingPoint[16].X = helpingPoint[2].X * D;
                    helpingPoint[16].Y = helpingPoint[2].Y * D;
                    helpingR[1] = distance(helpingPoint[13], helpingPoint[14]) * 2;
                }
                xmax = Math.Max(helpingPoint[0].X, helpingPoint[1].X);
                xmax = Math.Max(xmax, helpingPoint[2].X);
                xmax = Math.Max(xmax, helpingPoint[6].X);
                xmax = Math.Max(xmax, helpingPoint[7].X);
                xmax = Math.Max(xmax, helpingPoint[8].X);
                xmax = Math.Max(xmax, helpingPoint[12].X + helpingR[0] / 2);

                xmin = Math.Min(helpingPoint[0].X, helpingPoint[1].X);
                xmin = Math.Min(xmin, helpingPoint[2].X);
                xmin = Math.Min(xmin, helpingPoint[6].X);
                xmin = Math.Min(xmin, helpingPoint[7].X);
                xmin = Math.Min(xmin, helpingPoint[8].X);
                xmin = Math.Min(xmin, helpingPoint[12].X - helpingR[0] / 2);

                ymax = Math.Max(helpingPoint[0].Y, helpingPoint[1].Y);
                ymax = Math.Max(ymax, helpingPoint[2].Y);
                ymax = Math.Max(ymax, helpingPoint[6].Y);
                ymax = Math.Max(ymax, helpingPoint[7].Y);
                ymax = Math.Max(ymax, helpingPoint[8].Y);
                ymax = Math.Max(ymax, helpingPoint[12].Y + helpingR[0] / 2);

                ymin = Math.Min(helpingPoint[0].Y, helpingPoint[1].Y);
                ymin = Math.Min(ymin, helpingPoint[2].Y);
                ymin = Math.Min(ymin, helpingPoint[6].Y);
                ymin = Math.Min(ymin, helpingPoint[7].Y);
                ymin = Math.Min(ymin, helpingPoint[8].Y);
                ymin = Math.Min(ymin, helpingPoint[12].Y - helpingR[0] / 2);

                zoomFactor = Math.Min((canvasTriangle.ActualWidth - 60) / (xmax - xmin), (canvasTriangle.ActualHeight - 60) / (ymax - ymin));
                x2min = xmin * zoomFactor;
                x2max = xmax * zoomFactor;
                y2min = ymin * zoomFactor;
                y2max = ymax * zoomFactor;
                baseX = (canvasTriangle.ActualWidth - 60) / 2 - (x2max - x2min) / 2 - x2min + 30;
                baseY = (canvasTriangle.ActualHeight - 60) / 2 - (y2max - y2min) / 2 - y2min + 30;

                lineA.X1 = helpingPoint[1].X * zoomFactor + baseX; lineA.X2 = helpingPoint[2].X * zoomFactor + baseX;
                lineA.Y1 = helpingPoint[1].Y * zoomFactor + baseY; lineA.Y2 = helpingPoint[2].Y * zoomFactor + baseY;
                lineB.X1 = helpingPoint[2].X * zoomFactor + baseX; lineB.X2 = helpingPoint[0].X * zoomFactor + baseX;
                lineB.Y1 = helpingPoint[2].Y * zoomFactor + baseY; lineB.Y2 = helpingPoint[0].Y * zoomFactor + baseY;
                lineC.X1 = helpingPoint[0].X * zoomFactor + baseX; lineC.X2 = helpingPoint[1].X * zoomFactor + baseX;
                lineC.Y1 = helpingPoint[0].Y * zoomFactor + baseY; lineC.Y2 = helpingPoint[1].Y * zoomFactor + baseY;
                linemA.X1 = helpingPoint[0].X * zoomFactor + baseX; linemA.X2 = helpingPoint[3].X * zoomFactor + baseX;
                linemA.Y1 = helpingPoint[0].Y * zoomFactor + baseY; linemA.Y2 = helpingPoint[3].Y * zoomFactor + baseY;
                linemB.X1 = helpingPoint[1].X * zoomFactor + baseX; linemB.X2 = helpingPoint[4].X * zoomFactor + baseX;
                linemB.Y1 = helpingPoint[1].Y * zoomFactor + baseY; linemB.Y2 = helpingPoint[4].Y * zoomFactor + baseY;
                linemC.X1 = helpingPoint[2].X * zoomFactor + baseX; linemC.X2 = helpingPoint[5].X * zoomFactor + baseX;
                linemC.Y1 = helpingPoint[2].Y * zoomFactor + baseY; linemC.Y2 = helpingPoint[5].Y * zoomFactor + baseY;
                linehA.X1 = helpingPoint[0].X * zoomFactor + baseX; linehA.X2 = helpingPoint[6].X * zoomFactor + baseX;
                linehA.Y1 = helpingPoint[0].Y * zoomFactor + baseY; linehA.Y2 = helpingPoint[6].Y * zoomFactor + baseY;
                linehB.X1 = helpingPoint[1].X * zoomFactor + baseX; linehB.X2 = helpingPoint[7].X * zoomFactor + baseX;
                linehB.Y1 = helpingPoint[1].Y * zoomFactor + baseY; linehB.Y2 = helpingPoint[7].Y * zoomFactor + baseY;
                linehC.X1 = helpingPoint[2].X * zoomFactor + baseX; linehC.X2 = helpingPoint[8].X * zoomFactor + baseX;
                linehC.Y1 = helpingPoint[2].Y * zoomFactor + baseY; linehC.Y2 = helpingPoint[8].Y * zoomFactor + baseY;
                linepA.X1 = helpingPoint[0].X * zoomFactor + baseX; linepA.X2 = helpingPoint[9].X * zoomFactor + baseX;
                linepA.Y1 = helpingPoint[0].Y * zoomFactor + baseY; linepA.Y2 = helpingPoint[9].Y * zoomFactor + baseY;
                linepB.X1 = helpingPoint[1].X * zoomFactor + baseX; linepB.X2 = helpingPoint[10].X * zoomFactor + baseX;
                linepB.Y1 = helpingPoint[1].Y * zoomFactor + baseY; linepB.Y2 = helpingPoint[10].Y * zoomFactor + baseY;
                linepC.X1 = helpingPoint[2].X * zoomFactor + baseX; linepC.X2 = helpingPoint[11].X * zoomFactor + baseX;
                linepC.Y1 = helpingPoint[2].Y * zoomFactor + baseY; linepC.Y2 = helpingPoint[11].Y * zoomFactor + baseY;
                lineRA.X1 = helpingPoint[0].X * zoomFactor + baseX; lineRA.Y1 = helpingPoint[0].Y * zoomFactor + baseY;
                lineRB.X1 = helpingPoint[1].X * zoomFactor + baseX; lineRB.Y1 = helpingPoint[1].Y * zoomFactor + baseY;
                lineRC.X1 = helpingPoint[2].X * zoomFactor + baseX; lineRC.Y1 = helpingPoint[2].Y * zoomFactor + baseY;
                PointCollection myTrianglePoint = new PointCollection();
                myTrianglePoint.Add(new Point(lineC.X1, lineC.Y1));
                myTrianglePoint.Add(new Point(lineA.X1, lineA.Y1));
                myTrianglePoint.Add(new Point(lineB.X1, lineB.Y1));
                trianglecanvas.Points = myTrianglePoint;
                lineRA.X2 = lineRB.X2 = lineRC.X2 = helpingPoint[12].X * zoomFactor + baseX;
                lineRA.Y2 = lineRB.Y2 = lineRC.Y2 = helpingPoint[12].Y * zoomFactor + baseY;
                ellipseR.Width = ellipseR.Height = helpingR[0] * zoomFactor;
                Canvas.SetLeft(ellipseR, helpingPoint[12].X * zoomFactor + baseX - helpingR[0] * zoomFactor / 2);
                Canvas.SetTop(ellipseR, helpingPoint[12].Y * zoomFactor + baseY - helpingR[0] * zoomFactor / 2);
                linerA.X1 = helpingPoint[14].X * zoomFactor + baseX; linerA.Y1 = helpingPoint[14].Y * zoomFactor + baseY;
                linerB.X1 = helpingPoint[15].X * zoomFactor + baseX; linerB.Y1 = helpingPoint[15].Y * zoomFactor + baseY;
                linerC.X1 = helpingPoint[16].X * zoomFactor + baseX; linerC.Y1 = helpingPoint[16].Y * zoomFactor + baseY;
                linerA.X2 = linerB.X2 = linerC.X2 = helpingPoint[13].X * zoomFactor + baseX;
                linerA.Y2 = linerB.Y2 = linerC.Y2 = helpingPoint[13].Y * zoomFactor + baseY;
                ellipser.Width = ellipser.Height = helpingR[1] * zoomFactor;
                Canvas.SetLeft(ellipser, helpingPoint[13].X * zoomFactor + baseX - helpingR[1] * zoomFactor / 2);
                Canvas.SetTop(ellipser, helpingPoint[13].Y * zoomFactor + baseY - helpingR[1] * zoomFactor / 2);
                for (i = 0; i < 3; ++i)
                    for (j = 0; j < 2; ++j)
                    {
                        helpinglineh[i, j].X1 = helpingPoint[6 + i].X * zoomFactor + baseX;
                        helpinglineh[i, j].Y1 = helpingPoint[6 + i].Y * zoomFactor + baseY;
                        helpinglineh[i, j].X2 = helpingPoint[(i + j + 1) % 3].X * zoomFactor + baseX;
                        helpinglineh[i, j].Y2 = helpingPoint[(i + j + 1) % 3].Y * zoomFactor + baseY;
                    }
                Canvas.SetLeft(textAngleA, helpingPoint[0].X * zoomFactor + baseX - 5);
                Canvas.SetTop(textAngleA, helpingPoint[0].Y * zoomFactor + baseY - 15);
                Canvas.SetLeft(textAngleB, helpingPoint[1].X * zoomFactor + baseX - 10);
                Canvas.SetTop(textAngleB, helpingPoint[1].Y * zoomFactor + baseY);
                Canvas.SetLeft(textAngleC, helpingPoint[2].X * zoomFactor + baseX + 2);
                Canvas.SetTop(textAngleC, helpingPoint[2].Y * zoomFactor + baseY);
                Canvas.SetLeft(textSideA, helpingPoint[3].X * zoomFactor + baseX);
                Canvas.SetTop(textSideA, helpingPoint[3].Y * zoomFactor + baseY);
                Canvas.SetLeft(textSideB, helpingPoint[4].X * zoomFactor + baseX + 5);
                Canvas.SetTop(textSideB, helpingPoint[4].Y * zoomFactor + baseY - 10);
                Canvas.SetLeft(textSideC, helpingPoint[5].X * zoomFactor + baseX - 10);
                Canvas.SetTop(textSideC, helpingPoint[5].Y * zoomFactor + baseY - 10);
                Canvas.SetLeft(textmA, (helpingPoint[0].X + helpingPoint[3].X) / 2 * zoomFactor + baseX + 5);
                Canvas.SetTop(textmA, (helpingPoint[0].Y + helpingPoint[3].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(textmB, (helpingPoint[1].X + helpingPoint[4].X) / 2 * zoomFactor + baseX + 5);
                Canvas.SetTop(textmB, (helpingPoint[1].Y + helpingPoint[4].Y) / 2 * zoomFactor + baseY - 5);
                Canvas.SetLeft(textmC, (helpingPoint[2].X + helpingPoint[5].X) / 2 * zoomFactor + baseX - 15);
                Canvas.SetTop(textmC, (helpingPoint[2].Y + helpingPoint[5].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(texthA, (helpingPoint[0].X + helpingPoint[6].X) / 2 * zoomFactor + baseX + 5);
                Canvas.SetTop(texthA, (helpingPoint[0].Y + helpingPoint[6].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(texthB, (helpingPoint[1].X + helpingPoint[7].X) / 2 * zoomFactor + baseX + 5);
                Canvas.SetTop(texthB, (helpingPoint[1].Y + helpingPoint[7].Y) / 2 * zoomFactor + baseY - 5);
                Canvas.SetLeft(texthC, (helpingPoint[2].X + helpingPoint[8].X) / 2 * zoomFactor + baseX - 15);
                Canvas.SetTop(texthC, (helpingPoint[2].Y + helpingPoint[8].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(textpA, (helpingPoint[0].X + helpingPoint[9].X) / 2 * zoomFactor + baseX + 5);
                Canvas.SetTop(textpA, (helpingPoint[0].Y + helpingPoint[9].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(textpB, (helpingPoint[1].X + helpingPoint[10].X) / 2 * zoomFactor + baseX + 5);
                Canvas.SetTop(textpB, (helpingPoint[1].Y + helpingPoint[10].Y) / 2 * zoomFactor + baseY - 5);
                Canvas.SetLeft(textpC, (helpingPoint[2].X + helpingPoint[11].X) / 2 * zoomFactor + baseX - 15);
                Canvas.SetTop(textpC, (helpingPoint[2].Y + helpingPoint[11].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(textP, helpingPoint[13].X * zoomFactor + baseX);
                Canvas.SetTop(textP, helpingPoint[13].Y * zoomFactor + baseY - 10);
                Canvas.SetLeft(textS, helpingPoint[13].X * zoomFactor + baseX);
                Canvas.SetTop(textS, helpingPoint[13].Y * zoomFactor + baseY - 10);
                Canvas.SetLeft(textOutsideCircleR, (helpingPoint[0].X + helpingPoint[12].X) / 2 * zoomFactor + baseX - 15);
                Canvas.SetTop(textOutsideCircleR, (helpingPoint[0].Y + helpingPoint[12].Y) / 2 * zoomFactor + baseY);
                Canvas.SetLeft(textInsideCircleR, (helpingPoint[15].X + helpingPoint[13].X) / 2 * zoomFactor + baseX);
                Canvas.SetTop(textInsideCircleR, (helpingPoint[15].Y + helpingPoint[13].Y) / 2 * zoomFactor + baseY);
                rectLocker.Fill = null;
                textError.Foreground = null;
                if (calculated == 2) //with a red box and transparent
                {
                    Canvas.SetLeft(rectLocker, 0);
                    Canvas.SetTop(rectLocker, 0);
                    rectLocker.Width = canvasTriangle.ActualWidth;
                    rectLocker.Height = canvasTriangle.ActualHeight;
                    RectangleLockerBrush = new SolidColorBrush();
                    RectangleLockerBrush.Color = Color.FromArgb(210, 255, 255, 255);
                    rectLocker.Fill = RectangleLockerBrush;
                    TextErrorBrush = new SolidColorBrush();
                    TextErrorBrush.Color = Color.FromArgb(255, 220, 0, 0);
                    textError.Foreground = TextErrorBrush;
                    Canvas.SetLeft(textError, canvasTriangle.ActualWidth / 2 - textError.ActualWidth / 2);
                    Canvas.SetTop(textError, canvasTriangle.ActualHeight / 2 - textError.ActualHeight / 2);
                }
            }
            catch (Exception excep)
            {
                MessageBox.Show("Lỗi vẽ tam giác :" + excep.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private double convertAngleType(double value, int from, int to)
        {
            double res = value;
            if (from == 0) res = res * Math.PI / 180;
            if (from == 2) res = res * Math.PI / 200;
            if (to == 0) res = res * 180 / Math.PI;
            if (to == 2) res = res * 200 / Math.PI;
            return res;
        }
        private string formatConvertAngleType(double value, int from, int to)
        {
            double res = value;
            if (from == 0) res = res * Math.PI / 180;
            if (from == 2) res = res * Math.PI / 200;
            if (to == 0) res = res * 180 / Math.PI;
            if (to == 2) res = res * 200 / Math.PI;
            if (to == 1) //trying to format it in PI mode
            {
                int i, j;
                double ii;
                for (j = 1; j < 10; ++j)
                {
                    ii = j * res / Math.PI;
                    if (ii > 100) continue;
                    i = (int)ii;
                    if (Math.Abs(i - ii) <= Math.Abs(i * 1e-14)) //if i == ii or ii is int so we have fraction i/j
                    {
                        string strres = "";
                        if (i != 1) strres += i.ToString() + " ";
                        strres += "π";
                        if (j != 1) strres += " / " + j.ToString();
                        return strres;
                    }
                }
            }
            else res = Sensitive(res);
            return res.ToString();
        }
        private double Sensitive(double x)
        {
            double res,mbase,texp;
            int mexp;
            int flag;
            if (x == 0.0 || x == -0.0) res = 0.0;
            else
	        {
		        if (x < -0.0) {flag = 1;x = -x;} else flag = 0;
		        texp = Math.Log10(x);
		        if (texp >= 0.0) mexp = (int)Math.Floor(texp); else
		        {
			        texp = -texp;
			        mexp = (int)-Math.Ceiling(texp);
		        }
		        mbase = x / Math.Pow(10.0, mexp);
                res = Math.Floor(mbase * 1e+10 + 0.09) / 1e+10;
                if (Math.Abs(mbase - res) > 1e-12) res = x; else res = res * Math.Pow(10.0, mexp);
		        //there are two case of being smaller (res < mbase) : 1st: 0.00001 => 0 or 2nd: 1.34567 => 1.34. The different must be small
		        //so in the case of 1.34567 => 1.34, the result will be restore to its original
		        if (flag == 1) res = -res;
	        }
	        return res;
        }
        private double distance(Point A, Point B)
        {
            return Math.Sqrt((A.X - B.X) * (A.X - B.X) + (A.Y - B.Y) * (A.Y - B.Y));
        }
        private void generatelogs(List<Pair<int, int>> logs,List<string> variableList,Pair<int,double>[] outputvalue)
        {
            //~~~~ngôn ngữ tự nhiên
            foreach (Pair<int, int> pair in logs)
            {
                logsOfThinking += "Phương trình ";
                logsOfThinking += pair.First.ToString() + " ";
                if (pair.First < Int32.Parse(expressionSettings[0]))
                    logsOfThinking += expressionSettings[pair.First + 3];
                else
                {
                    logsOfThinking += expressionProfiles[pair.First + 3 - Int32.Parse(expressionSettings[0])];
                }
                logsOfThinking += " => yếu tố ";
                logsOfThinking += variableList[pair.Second];
                logsOfThinking += System.Environment.NewLine;
            }
        }
        private void calculation()
        {
            List<Pair<int, int>> logs = new List<Pair<int, int>>();
            Dictionary<string, int> variableID = new Dictionary<string, int>();
            List<string> variableID2 = new List<string>();
            int[,] relationshipDataTable;
            int[,] relationshipDataTableTracker;
            int[] relationshipDataTableSumRow;
            int[] relationshipDataTableActualRow;
            Pair<int,double>[] outputvalue;
            Pair<int,double>[] testoutputvalue;
            Parser[] function;
            double[] testelements = new double[elements.Length];
            string[] VariableListFunction;
            string[] exceptionReading;
            int unknownVars = 0;
            int i, j, k;
            int nx,changed;
            int nFunction, nVar = elements.Length, nFunction1, nFunction2, nException1, nException2, nAssignment1, nAssignment2, nExceptionType1,nExceptionType2,nExceptionImpact;
            double res = 0.0;
            double anglelowerbound,angleupperbound;

            nFunction1 = Int32.Parse(expressionSettings[0]);
            nFunction2 = Int32.Parse(expressionProfiles[0]);
            nException1 = Int32.Parse(expressionSettings[1]);
            nException2 = Int32.Parse(expressionProfiles[1]);
            nAssignment1 = Int32.Parse(expressionSettings[2]);
            nAssignment2 = Int32.Parse(expressionProfiles[2]);
            nFunction = nFunction1 + nFunction2;
            function = new Parser[nFunction];
            for (i = 0; i < nFunction1; ++i)
            {
                function[i] = new Parser();
                function[i].inputExpression(expressionSettings[i + 3]);
            }
            for (j = 0; j < nFunction2; ++j, ++i)
            {
                function[i] = new Parser();
                function[i].inputExpression(expressionProfiles[j + 3]);
            }
            variableID.Add("a", 0);
            variableID.Add("b", 1);
            variableID.Add("c", 2);
            variableID.Add("A", 3);
            variableID.Add("B", 4);
            variableID.Add("C", 5);
            variableID.Add("mA", 6);
            variableID.Add("mB", 7);
            variableID.Add("mC", 8);
            variableID.Add("hA", 9);
            variableID.Add("hB", 10);
            variableID.Add("hC", 11);
            variableID.Add("pA", 12);
            variableID.Add("pB", 13);
            variableID.Add("pC", 14);
            variableID.Add("p", 15);
            variableID.Add("S", 16);
            variableID.Add("R", 17);
            variableID.Add("r", 18);
            variableID2.Add("a");
            variableID2.Add("b");
            variableID2.Add("c");
            variableID2.Add("A");
            variableID2.Add("B");
            variableID2.Add("C");
            variableID2.Add("mA");
            variableID2.Add("mB");
            variableID2.Add("mC");
            variableID2.Add("hA");
            variableID2.Add("hB");
            variableID2.Add("hC");
            variableID2.Add("pA");
            variableID2.Add("pB");
            variableID2.Add("pC");
            variableID2.Add("p");
            variableID2.Add("S");
            variableID2.Add("R");
            variableID2.Add("r");
            for (i = 0; i < nFunction; ++i)
            {
                VariableListFunction = function[i].VariableList2;
                for (j = 0;j < VariableListFunction.Length;++j)
                    if (!variableID.ContainsKey(VariableListFunction[j]))
                    {
                        variableID.Add(VariableListFunction[j], nVar);
                        variableID2.Add(VariableListFunction[j]);
                        ++nVar;
                    }
            }
            relationshipDataTable = new int[nFunction, nVar];
            for (i = 0; i < nFunction; ++i)
            {
                VariableListFunction = function[i].VariableList2;
                for (j = 0; j < VariableListFunction.Length; ++j)
                {
                    relationshipDataTable[i, variableID[VariableListFunction[j]]] = 1;
                }
            }
            for (i = 0,j = nFunction1 + 3; i < nException1; ++i,++j)
            {
                exceptionReading = expressionSettings[j].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                nExceptionType1 = Int32.Parse(exceptionReading[0]);
                nExceptionType2 = Int32.Parse(exceptionReading[1]);
                nExceptionImpact = Int32.Parse(exceptionReading[2]);
                for (k = 0;k < nExceptionImpact;++k)
                {
                    int l;
                    int kk = Int32.Parse(exceptionReading[3 + nExceptionType1 + nExceptionType2 + k]);
                    for (l = 0; l < nExceptionType1; ++l)
                    {
                        relationshipDataTable[kk, variableID[exceptionReading[l + 3]]] = -1;
                    }
                    for (l = 0; l < nExceptionType2; ++l)
                    {
                        relationshipDataTable[kk, variableID[exceptionReading[l + nExceptionType1 + 3]]] = -1;
                    }
                }
            }
            for (i = 0, j = nFunction2 + 3; i < nException2; ++i, ++j)
            {
                exceptionReading = expressionProfiles[j].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                nExceptionType1 = Int32.Parse(exceptionReading[0]);
                nExceptionType2 = Int32.Parse(exceptionReading[1]);
                nExceptionImpact = Int32.Parse(exceptionReading[2]);
                for (k = 0; k < nExceptionImpact; ++k)
                {
                    int l;
                    int kk = Int32.Parse(exceptionReading[3 + nExceptionType1 + nExceptionType2 + k]) + nFunction1;
                    for (l = 0; l < nExceptionType1; ++l)
                    {
                        relationshipDataTable[kk, variableID[exceptionReading[l + 3]]] = -1;
                    }
                    for (l = 0; l < nExceptionType2; ++l)
                    {
                        relationshipDataTable[kk, variableID[exceptionReading[l + nExceptionType1 + 3]]] = -1;
                    }
                }
            }
            if (nAssignment1 != 0)
            {
                exceptionReading = expressionSettings[nFunction1 + nException1 + 3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                for (i = 0; i < exceptionReading.Length; ++i)
                {
                    int kk = Int32.Parse(exceptionReading[i]);
                    if (function[kk].EquationType != 5) throw new Exception("Biểu thức thứ " + kk.ToString() + "của phần Settings không phải là biểu thức gán hợp lệ");
                    VariableListFunction = function[kk].VariableList2;
                    for (j = 1; j < VariableListFunction.Length; ++j)
                    {
                        relationshipDataTable[kk, variableID[VariableListFunction[j]]] = -1;
                    }
                }
            }
            if (nAssignment2 != 0)
            {
                exceptionReading = expressionProfiles[nFunction2 + nException2 + 3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                for (i = 0; i < exceptionReading.Length; ++i)
                {
                    int kk = Int32.Parse(exceptionReading[i]) + nFunction1;
                    if (function[kk].EquationType != 5) throw new Exception("Biểu thức thứ " + (kk - nFunction1).ToString() + " của phần Profiles không phải là biểu thức gán hợp lệ");
                    VariableListFunction = function[kk].VariableList2;
                    for (j = 1; j < VariableListFunction.Length; ++j)
                    {
                        relationshipDataTable[kk, variableID[VariableListFunction[j]]] = -1;
                    }
                }
            }
            relationshipDataTableTracker = (int [,])relationshipDataTable.Clone();
            relationshipDataTableSumRow = new int[nFunction];
            relationshipDataTableActualRow = new int[nFunction];
            for (i = 0; i < nFunction; ++i)
                for (j = 0; j < elements.Length; ++j)
                    if (relationshipDataTableTracker[i, j] != 0)
                    {
                        ++relationshipDataTableSumRow[i];
                        if (isInputElement[j] == 1)
                        {
                            ++relationshipDataTableActualRow[i];
                            relationshipDataTableTracker[i, j] = 0;
                        }
                    }
            outputvalue = new Pair<int,double>[variableID2.Count];
            for (i = 0; i < elements.Length; ++i)
            {
                if (isInputElement[i] == 0) ++unknownVars;
                outputvalue[i] = new Pair<int, double>();
                outputvalue[i].First = isInputElement[i];
                outputvalue[i].Second = elements[i];
            }
            for (; i < outputvalue.Length; ++i)
            {
                ++unknownVars;
                outputvalue[i] = new Pair<int, double>();
                outputvalue[i].First = 0;
                outputvalue[i].Second = 0.0;
            }
                
            while (unknownVars > 0)
            {
                changed = 0;
                nx = 0;
                for (i = 0; i < nFunction; ++i)
                {
                    if (relationshipDataTableActualRow[i] == relationshipDataTableSumRow[i] - 1)
                        for (j = 0; j < nVar; ++j)
                            //if (relationshipDataTableTracker[i, j] == 1 || ((relationshipDataTableTracker[i, j] == -1) && (j == 3 || j == 4 || j == 5) && (angleexceptiontype[j - 3] != 0)))
                            if (relationshipDataTableTracker[i, j] == 1)
                            {
                                function[i].replaceVariableSystem(variableID, outputvalue);
                                if (j == 3 || j == 4 || j == 5)
                                {
                                    anglelowerbound = 0.0; angleupperbound = Math.PI;
                                    if (angleexceptiontype[j - 3] == 1) angleupperbound = Math.PI / 2;
                                    else if (angleexceptiontype[j - 3] == 2) anglelowerbound = Math.PI / 2;
                                    nx = function[i].SolveInRange(0, anglelowerbound, angleupperbound, 4, ref res);
                                } else nx = function[i].Solve(0.0, ref res);
                                if (nx == 0) break;
                                changed = 1;
                                outputvalue[j].First = 1;
                                outputvalue[j].Second = res;
                                --unknownVars;
                                logs.Add(new Pair<int, int>(i, j));
                                for (k = 0; k < nFunction; ++k)
                                    if (relationshipDataTableTracker[k, j] != 0)
                                    {
                                        relationshipDataTableTracker[k, j] = 0;
                                        ++relationshipDataTableActualRow[k];
                                    }
                                break;
                            }
                }
                if (changed == 0) break;
            }
            for (i = 0; i < elements.Length; ++i) { elements[i] = outputvalue[i].Second; isOutputElement[i] = outputvalue[i].First; }
            generatelogs(logs, variableID2,outputvalue);

            //check for conflict
            if (unknownVars > 0)
            {
                calculated = 2;
                isErrorElement = new int[elements.Length];
                for (i = 0; i < isErrorElement.Length; ++i)
                    if (isOutputElement[i] == 0) isErrorElement[i] = 1; else isErrorElement[i] = 0;
            }
            else
            {
                unknownVars = 0;
                testelements[0] = elements[0];
                testelements[1] = elements[1];
                testelements[2] = elements[2];
                relationshipDataTableTracker = (int[,])relationshipDataTable.Clone();
                relationshipDataTableActualRow = new int[nFunction];
                for (i = 0; i < nFunction; ++i)
                    for (j = 0; j < 3; ++j)
                        if (relationshipDataTableTracker[i, j] != 0)
                        {
                            ++relationshipDataTableActualRow[i];
                            relationshipDataTableTracker[i, j] = 0;
                        }
                testoutputvalue = new Pair<int,double>[variableID2.Count];
                for (i = 0; i < 3; ++i)
                {
                    testoutputvalue[i] = new Pair<int, double>();
                    testoutputvalue[i].First = 1;
                    testoutputvalue[i].Second = testelements[i];
                }
                for (; i < testoutputvalue.Length; ++i)
                {
                    ++unknownVars;
                    testoutputvalue[i] = new Pair<int, double>();
                    testoutputvalue[i].First = 0;
                    testoutputvalue[i].Second = 0.0;
                }
                while (unknownVars > 0)
                {
                    changed = 0;
                    nx = 0;
                    for (i = 0; i < nFunction; ++i)
                    {
                        if (relationshipDataTableActualRow[i] == relationshipDataTableSumRow[i] - 1)
                            for (j = 0; j < nVar; ++j)
                                //if (relationshipDataTableTracker[i, j] == 1 || ((j == 3 || j == 4 || j == 5) && (angleexceptiontype[j - 3] != 0)))
                                if (relationshipDataTableTracker[i, j] == 1)
                                {
                                    function[i].replaceVariableSystem(variableID, testoutputvalue);
                                    if (j == 3 || j == 4 || j == 5)
                                    {
                                        anglelowerbound = 0.0; angleupperbound = Math.PI;
                                        if (angleexceptiontype[j - 3] == 1) angleupperbound = Math.PI / 2;
                                        else if (angleexceptiontype[j - 3] == 2) anglelowerbound = Math.PI / 2;
                                        nx = function[i].SolveInRange(0, anglelowerbound, angleupperbound, 4, ref res);
                                    }
                                    else nx = function[i].Solve(0.0, ref res);
                                    if (nx == 0) break;
                                    changed = 1;
                                    testoutputvalue[j].First = 1;
                                    testoutputvalue[j].Second = res;
                                    --unknownVars;
                                    for (k = 0; k < nFunction; ++k)
                                        if (relationshipDataTableTracker[k, j] != 0)
                                        {
                                            relationshipDataTableTracker[k, j] = 0;
                                            ++relationshipDataTableActualRow[k];
                                        }
                                    break;
                                }
                    }
                    if (changed == 0) break;
                }
                calculated = 1;
                for (i = 0; i < elements.Length; ++i) //recheck if equal
                    if (Math.Abs(testoutputvalue[i].Second - outputvalue[i].Second) > Math.Abs(testoutputvalue[i].Second * 1e-8))
                    {
                        isErrorElement[i] = 1;
                        calculated = 2;
                    }
                    else isErrorElement[i] = 0;
                //check if negative
                for (i = 0; i < elements.Length; ++i)
                    if (elements[i] < -0.0)
                    {
                        isErrorElement[i] = 1;
                        calculated = 2;
                    }
                //check if angle is illegal
                if (elements[3] + elements[4] + elements[5] > Math.PI + 1e-8)
                {
                    isErrorElement[3] = isErrorElement[4] = isErrorElement[5] = 1;
                    calculated = 2;
                }
                //check if the side is illegal
                if (elements[0] + elements[1] <= elements[2] || Math.Abs(elements[0] - elements[1]) >= elements[2])
                {
                    isErrorElement[0] = isErrorElement[1] = isErrorElement[2] = 1;
                    calculated = 2;
                }
                //check type of the triangle
                int isosceles = 0,right = 0,obtuse = 0;
                if (triangleType != -1)
                {
                    triangleType = 0;
                    if (Math.Abs(elements[3] - Math.PI / 3) <= (elements[3] * 1e-8) && Math.Abs(elements[4] - Math.PI / 3) <= (elements[4] * 1e-8) && Math.Abs(elements[5] - Math.PI / 3) <= (elements[5] * 1e-8))
                        triangleType = 2;
                    else
                    {
                        for (i = 3; i < 6; ++i)
                        {
                            if (Math.Abs(elements[i + 1] - elements[i + 2]) <= (elements[i + 1] * 1e-8))
                                isosceles = 1;
                            if (Math.Abs(elements[i] - Math.PI / 2) <= (elements[i] * 1e-8))
                                right = 1;
                            else if (elements[i] > Math.PI / 2)
                                obtuse = 1;
                            if (obtuse == 1)
                            {
                                if (isosceles == 1) triangleType = 6; else triangleType = 5;
                                triangleTypeAngle = i - 3;
                                break;
                            }
                            else if (right == 1)
                            {
                                if (isosceles == 1) triangleType = 5; else triangleType = 4;
                                if (Math.Abs(elements[(i + 1) % 3 + 3] - Math.PI / 3) <= (elements[(i + 1) % 3 + 3] * 1e-8))
                                {
                                    triangleType = 7;
                                }
                                else if (Math.Abs(elements[(i + 1) % 3 + 3] - Math.PI / 6) <= (elements[(i + 1) % 3 + 3] * 1e-8))
                                {
                                    triangleType = 8;
                                }
                                triangleTypeAngle = i - 3;
                                break;
                            }
                            else if (isosceles == 1)
                            {
                                triangleType = 2;
                                triangleTypeAngle = i - 3;
                                break;
                            }
                        }
                        if (triangleType == 0) triangleType = 1;
                    }
                }
                if (calculated == 2)
                {
                    if (isInputElement[0] == 0) isErrorElement[0] = 1;
                    if (isInputElement[1] == 0) isErrorElement[1] = 1;
                    if (isInputElement[2] == 0) isErrorElement[2] = 1;
                }
            }
        }
        private void GotFocusPaintingLabel(object sender)
        {
            System.Windows.Media.LinearGradientBrush BackgroundPainter = new LinearGradientBrush();
            System.Windows.Media.SolidColorBrush BorderPainter = new SolidColorBrush();
            BackgroundPainter.StartPoint = new Point(0.5, 0);
            BackgroundPainter.EndPoint = new Point(0.5, 1);
            if (highlightElementCanvas == highlightElementCanvasPrevious) highlightElementCanvasPrevious = -1;
            if (calculated == 2 && (isErrorElement[highlightElementCanvas] == 1))
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 255, 127, 127), 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 255, 127, 127), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 220, 1, 1), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 220, 1, 1), 1.0));
                BorderPainter.Color = Color.FromArgb(255, 213, 0, 0);
            }
            else
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 143, 186, 228), 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 143, 186, 228), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 48, 114, 181), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 48, 114, 181), 1.0));
                BorderPainter.Color = Color.FromArgb(255, 51, 108, 205);
            }
            ((Border)sender).Background = BackgroundPainter;
            ((Border)sender).BorderBrush = BorderPainter;
            canvasDraw(0);
        }
        private void LostFocusPaintingLabel(object sender)
        {
            System.Windows.Media.SolidColorBrush Painter = new SolidColorBrush();
            if (calculated == 2 && (isErrorElement[highlightElementCanvasPrevious] == 1))
            {
                Painter.Color = Color.FromArgb(255, 255, 167, 167);
            }
            else
            {
                Painter.Color = Color.FromArgb(255, 178, 207, 246);
            }
            ((Border)sender).Background = Painter;
            ((Border)sender).BorderBrush = Painter;
            canvasDraw(0);
        }
        private object getBorderFromTextBox(TextBox sender){return ((Grid)(sender.Parent)).Parent;}
        private void PaintingBeforeCalculate(TextBox sender)
        {
            SolidColorBrush BlueText = new SolidColorBrush();
            BlueText.Color = Color.FromArgb(255, 50, 127, 204);
            LostFocusPaintingLabel(getBorderFromTextBox(sender));
            sender.IsReadOnly = false;
            sender.Background = SystemColors.ControlLightLightBrush;
            sender.Foreground = BlueText;
            sender.Text = "";
        }
        private void PaintingAfterCalculate(TextBox sender)
        {
            SolidColorBrush GreenText = new SolidColorBrush();
            GreenText.Color = Colors.Green;
            LostFocusPaintingLabel(getBorderFromTextBox(sender));
            if (isInputElement[highlightElementCanvasPrevious] == 0)
            {
                sender.IsReadOnly = true;
                sender.Background = SystemColors.ControlLightBrush;
                sender.Foreground = GreenText;
            }
            if (isOutputElement[highlightElementCanvasPrevious] == 1)
            {
                if (highlightElementCanvasPrevious == 3 || highlightElementCanvasPrevious == 4 || highlightElementCanvasPrevious == 5)
                {
                    sender.Text = formatConvertAngleType(elements[highlightElementCanvasPrevious], 1, angleMode);
                }
                else
                {
                    sender.Text = elements[highlightElementCanvasPrevious].ToString();
                }
            }
        }
        private void PaintingBoxBeforeCalculate(Button sender)
        {
            SolidColorBrush BackgroundPainter = new SolidColorBrush();
            BackgroundPainter.Color = Color.FromArgb(255, 178, 207, 236);
            sender.Background = BackgroundPainter;
            sender.BorderBrush = null;
            btnTriangleTypeAngle.Background = BackgroundPainter;
            btnTriangleTypeAngle.BorderBrush = null;
            if (original == 0)
            {
                System.Windows.Media.LinearGradientBrush BackgroundPainter2 = new LinearGradientBrush();
                System.Windows.Media.SolidColorBrush BorderPainter = new SolidColorBrush();
                BackgroundPainter2.StartPoint = new Point(0.5, 0);
                BackgroundPainter2.EndPoint = new Point(0.5, 1);
                BackgroundPainter2.GradientStops.Add(new GradientStop(Colors.LightGray, 0.0));
                BackgroundPainter2.GradientStops.Add(new GradientStop(Colors.LightGray, 0.5));
                BackgroundPainter2.GradientStops.Add(new GradientStop(Colors.Gray, 0.5));
                BackgroundPainter2.GradientStops.Add(new GradientStop(Colors.Gray, 1.0));
                BorderPainter.Color = Colors.Gray;
                btnTriangleType.Background = BackgroundPainter2;
                btnTriangleType.BorderBrush = BorderPainter;
            }
        }
        private void PaintingBoxAfterCalculate(Button sender)
        {
            System.Windows.Media.LinearGradientBrush BackgroundPainter = new LinearGradientBrush();
            System.Windows.Media.SolidColorBrush BorderPainter = new SolidColorBrush();
            BackgroundPainter.StartPoint = new Point(0.5, 0);
            BackgroundPainter.EndPoint = new Point(0.5, 1);
            if (calculated == 2)
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 255, 127, 127), 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 255, 127, 127), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 220, 1, 1), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 220, 1, 1), 1.0));
                BorderPainter.Color = Color.FromArgb(255, 213, 0, 0);
                sender.Background = BackgroundPainter;
                sender.BorderBrush = BorderPainter;
                sender.Content = "Unknown";
                ColumnTriangleTypeAngle.Width = (GridLength) (new GridLengthConverter()).ConvertFromString("0");
                return;
            }
            sender.Content = triangleName[triangleType];
            btnTriangleTypeAngle.Content = triangleAngleType[triangleTypeAngle];
            if (triangleType != inputTriangleType)
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.LightGreen, 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.LightGreen, 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.Green, 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Colors.Green, 1.0));
                BorderPainter.Color = Colors.Green;
            }
            else
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 143, 186, 228), 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 143, 186, 228), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 48, 114, 181), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 48, 114, 181), 1.0));
                BorderPainter.Color = Color.FromArgb(255, 51, 108, 205);
            }
            sender.Background = BackgroundPainter;
            sender.BorderBrush = BorderPainter;
            btnTriangleTypeAngle.Background = BackgroundPainter;
            btnTriangleTypeAngle.BorderBrush = BorderPainter;
            if (triangleType < 2)
                ColumnTriangleTypeAngle.Width = (GridLength)(new GridLengthConverter()).ConvertFromString("0");
            else
                ColumnTriangleTypeAngle.Width = (GridLength)(new GridLengthConverter()).ConvertFromString("28");
        }
        private void labelSideA_MouseDown(object sender, MouseButtonEventArgs e) { rtbSideA.Focus(); }
        private void labelSideB_MouseDown(object sender, MouseButtonEventArgs e) { rtbSideB.Focus(); }
        private void labelSideC_MouseDown(object sender, MouseButtonEventArgs e) { rtbSideC.Focus(); }
        private void labelAngleA_MouseDown(object sender, MouseButtonEventArgs e) { rtbAngleA.Focus(); }
        private void labelAngleB_MouseDown(object sender, MouseButtonEventArgs e) { rtbAngleB.Focus(); }
        private void labelAngleC_MouseDown(object sender, MouseButtonEventArgs e) { rtbAngleC.Focus(); }
        private void labelmA_MouseDown(object sender, MouseButtonEventArgs e) { rtbmA.Focus(); }
        private void labelmB_MouseDown(object sender, MouseButtonEventArgs e) { rtbmB.Focus(); }
        private void labelmC_MouseDown(object sender, MouseButtonEventArgs e) { rtbmC.Focus(); }
        private void labelhA_MouseDown(object sender, MouseButtonEventArgs e) { rtbhA.Focus(); }
        private void labelhB_MouseDown(object sender, MouseButtonEventArgs e) { rtbhB.Focus(); }
        private void labelhC_MouseDown(object sender, MouseButtonEventArgs e) { rtbhC.Focus(); }
        private void labelpA_MouseDown(object sender, MouseButtonEventArgs e) { rtbpA.Focus(); }
        private void labelpB_MouseDown(object sender, MouseButtonEventArgs e) { rtbpB.Focus(); }
        private void labelpC_MouseDown(object sender, MouseButtonEventArgs e) { rtbpC.Focus(); }
        private void labelP_MouseDown(object sender, MouseButtonEventArgs e) { rtbP.Focus(); }
        private void labelS_MouseDown(object sender, MouseButtonEventArgs e) { rtbS.Focus(); }
        private void labelOutsideCircleR_MouseDown(object sender, MouseButtonEventArgs e) { rtbOutsideCircleR.Focus(); }
        private void labelInsideCircleR_MouseDown(object sender, MouseButtonEventArgs e) { rtbInsideCircleR.Focus(); }
        private void rtbSideA_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 0; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbSideB_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 1; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbSideC_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 2; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbAngleA_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 3; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbAngleB_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 4; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbAngleC_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 5; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbmA_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 6; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbmB_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 7; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbmC_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 8; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbhA_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 9; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbhB_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 10; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbhC_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 11; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbpA_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 12; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbpB_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 13; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbpC_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 14; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbP_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 15; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbS_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 16; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbOutsideCircleR_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 17; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbInsideCircleR_GotFocus(object sender, RoutedEventArgs e) { highlightElementCanvas = 18; GotFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }

        private void rtbSideA_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 0; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbSideB_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 1; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbSideC_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 2; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbAngleA_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 3; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbAngleB_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 4; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbAngleC_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 5; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbmA_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 6; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbmB_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 7; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbmC_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 8; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbhA_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 9; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbhB_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 10; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbhC_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 11; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbpA_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 12; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbpB_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 13; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbpC_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 14; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbP_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 15; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbS_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 16; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbOutsideCircleR_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 17; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }
        private void rtbInsideCircleR_LostFocus(object sender, RoutedEventArgs e) { highlightElementCanvasPrevious = 18; LostFocusPaintingLabel(((Grid)((TextBox)sender).Parent).Parent); }

        private void btnTriangleType_MouseEnter(object sender, MouseEventArgs e)
        {
            System.Windows.Media.LinearGradientBrush BackgroundPainter = new LinearGradientBrush();
            System.Windows.Media.SolidColorBrush BorderPainter = new SolidColorBrush();
            BackgroundPainter.StartPoint = new Point(0.5, 0);
            BackgroundPainter.EndPoint = new Point(0.5, 1);
            if (calculated == 2)
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 255, 127, 127), 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 255, 127, 127), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 220, 1, 1), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 220, 1, 1), 1.0));
                BorderPainter.Color = Color.FromArgb(255, 213, 0, 0);
            }
            else
            {
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 143, 186, 228), 0.0));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 143, 186, 228), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 48, 114, 181), 0.5));
                BackgroundPainter.GradientStops.Add(new GradientStop(Color.FromArgb(255, 48, 114, 181), 1.0));
                BorderPainter.Color = Color.FromArgb(255, 51, 108, 205);
            }
            ((Button)sender).Background = BackgroundPainter;
            ((Button)sender).BorderBrush = BorderPainter;
        }
        private void btnTriangleType_MouseLeave(object sender, MouseEventArgs e)
        {
            System.Windows.Media.SolidColorBrush Painter = new SolidColorBrush();
            if (calculated == 2)
            {
                Painter.Color = Color.FromArgb(255, 255, 167, 167);
            }
            else
            {
                Painter.Color = Color.FromArgb(255, 178, 207, 246);
            }
            ((Button)sender).Background = Painter;
            ((Button)sender).BorderBrush = Painter;
        }

        private void btnTriangleType_Click(object sender, RoutedEventArgs e)
        {
            if (calculated == 1 || calculated == 2) return;
            if (triangleType == -1) return;
            triangleType = (triangleType + 1) % triangleName.Length;
            ((Button)sender).Content = triangleName[triangleType];

            GridLengthConverter myGridLengthConverter = new GridLengthConverter();
            if (triangleType == 0 || triangleType == 1 || triangleType == 2)
            {
                ColumnTriangleTypeAngle.Width = (GridLength)myGridLengthConverter.ConvertFromString("0");
            }
            else
            {
                ColumnTriangleTypeAngle.Width = (GridLength)myGridLengthConverter.ConvertFromString("28");
            }
            setupAfterTriangleSetting();
        }
        private void btnTriangleTypeAngle_Click(object sender, RoutedEventArgs e)
        {
            if (calculated == 1 || calculated == 2) return;
            triangleTypeAngle = (triangleTypeAngle + 1) % triangleAngleType.Length;
            ((Button)sender).Content = triangleAngleType[triangleTypeAngle];
            setupAfterTriangleSetting();
        }
        private void resetTriangleSettings()
        {
            highlightElementCanvas = -1;
            highlightElementCanvasPrevious = 0;
            PaintingBeforeCalculate(rtbSideA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbSideB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbSideC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbAngleC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbmC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbhC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpA); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpB); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbpC); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbP); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbS); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbOutsideCircleR); ++highlightElementCanvasPrevious;
            PaintingBeforeCalculate(rtbInsideCircleR); ++highlightElementCanvasPrevious;
            highlightElementCanvasPrevious = -1;
        }
        private void setupAfterTriangleSetting()
        {
            //add the exception angle
            //add more rule
            //restriction on input
            //add checking rule
            resetTriangleSettings();
            //reset rule

            switch (triangleType)
            {
                case 0: //Nothing
                    angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 0;
                    break;
                case 1: //Sin is OK
                    angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 1;
                    break;
                case 2: //Every one is the same = 60
                    rtbAngleA.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                    rtbAngleA.IsReadOnly = true;
                    rtbAngleA.Background = SystemColors.ControlLightBrush;
                    rtbAngleB.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                    rtbAngleB.IsReadOnly = true;
                    rtbAngleB.Background = SystemColors.ControlLightBrush;
                    rtbAngleC.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                    rtbAngleC.IsReadOnly = true;
                    rtbAngleC.Background = SystemColors.ControlLightBrush;
                    break;
                case 3://Activate Sin
                    angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 1;
                    switch (triangleTypeAngle)
                    {
                        case 0:
                            angleexceptiontype[0] = 0;
                            break;
                        case 1:
                            angleexceptiontype[1] = 0;
                            break;
                        case 2:
                            angleexceptiontype[2] = 0;
                            break;
                    }
                    break;
                case 4: //Angle is 90, activate sin
                    angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 1;
                    switch (triangleTypeAngle)
                    {
                        case 0:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleA.IsReadOnly = true;
                            rtbAngleA.Background = SystemColors.ControlLightBrush;
                            break;
                        case 1:
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleB.IsReadOnly = true;
                            rtbAngleB.Background = SystemColors.ControlLightBrush;
                            break;
                        case 2:
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleC.IsReadOnly = true;
                            rtbAngleC.Background = SystemColors.ControlLightBrush;
                            break;
                    }
                    break;
                case 5://Angle is 90, exclude 1 side activate sin
                    rtbAngleA.Text = formatConvertAngleType(Math.PI / 4, 1, angleMode);
                    rtbAngleA.IsReadOnly = true;
                    rtbAngleA.Background = SystemColors.ControlLightBrush;
                    rtbAngleB.Text = formatConvertAngleType(Math.PI / 4, 1, angleMode);
                    rtbAngleB.IsReadOnly = true;
                    rtbAngleB.Background = SystemColors.ControlLightBrush;
                    rtbAngleC.Text = formatConvertAngleType(Math.PI / 4, 1, angleMode);
                    rtbAngleC.IsReadOnly = true;
                    rtbAngleC.Background = SystemColors.ControlLightBrush;
                    switch (triangleTypeAngle)
                    {
                        case 0:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            break;
                        case 1:
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            break;
                        case 2:
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            break;
                    }
                    break;
                case 6://1 Angle > 90, activate sin
                    angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 1;
                    switch (triangleTypeAngle)
                    {
                        case 0:
                            angleexceptiontype[0] = 2;
                            break;
                        case 1:
                            angleexceptiontype[1] = 2;
                            break;
                        case 2:
                            angleexceptiontype[2] = 2;
                            break;
                    }
                    break;
                case 7://Angle is 90 60 30, add some rule
                    rtbAngleA.IsReadOnly = true;
                    rtbAngleA.Background = SystemColors.ControlLightBrush;
                    rtbAngleB.IsReadOnly = true;
                    rtbAngleB.Background = SystemColors.ControlLightBrush;
                    rtbAngleC.IsReadOnly = true;
                    rtbAngleC.Background = SystemColors.ControlLightBrush;
                    switch (triangleTypeAngle)
                    {
                        case 0:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 6, 1, angleMode);
                            break;
                        case 1:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 6, 1, angleMode);
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                            break;
                        case 2:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 6, 1, angleMode);
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            break;
                    }
                    break;
                case 8://Another Angle is 90 30 60, add some rule
                    rtbAngleA.IsReadOnly = true;
                    rtbAngleA.Background = SystemColors.ControlLightBrush;
                    rtbAngleB.IsReadOnly = true;
                    rtbAngleB.Background = SystemColors.ControlLightBrush;
                    rtbAngleC.IsReadOnly = true;
                    rtbAngleC.Background = SystemColors.ControlLightBrush;
                    switch (triangleTypeAngle)
                    {
                        case 0:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 6, 1, angleMode);
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                            break;
                        case 1:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 6, 1, angleMode);
                            break;
                        case 2:
                            rtbAngleA.Text = formatConvertAngleType(Math.PI / 6, 1, angleMode);
                            rtbAngleB.Text = formatConvertAngleType(Math.PI / 3, 1, angleMode);
                            rtbAngleC.Text = formatConvertAngleType(Math.PI / 2, 1, angleMode);
                            break;
                    }
                    break;
                case -1: //Custom
                    angleexceptiontype[0] = angleexceptiontype[1] = angleexceptiontype[2] = 0;
                    break;
                default: throw new Exception("Loại tam giác không hợp lệ");
            }
            canvasDraw(1);
        }

    }
}
