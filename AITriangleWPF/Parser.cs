﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AITriangleWPF
{
    class Parser
    {
        private Object[] expression;
        private Dictionary<string, int> variableList;
        private string[] variableList2;
        public string[] VariableList2 { get { return variableList2; } }
        public int EquationType { get { return equationType; } }
        private Pair<int,double>[] variableVal;
        private static Dictionary<string, int> operatorPriorityList;
        private static bool initialized = false;
        private string detectedVar;
        private double dynamicEps;
        private int equationType = 0;//0 = equation,1 = less than, 2 = less than or equal, 3 = greater, 4 = greater or equal, 5 (auto) = the equation's left hand has one variable so this is a formula
        private bool isalnum(char c)
        {
            if (isdigit(c)) return true;
            if (isanpha(c)) return true;
            return false;
        }
        private bool isdigit(char c)
        {
            if (c <= '9' && c >= '0') return true;
            if (c == '.') return true;
            return false;
        }
        private bool isanpha(char c)
        {
            if (c <= 'Z' && c >= 'A') return true;
            if (c <= 'z' && c >= 'a') return true;
            return false;
        }
        private void Initializing()
        {
            initialized = true;
            operatorPriorityList = new Dictionary<string, int>();
            operatorPriorityList.Add("sqrt", 1);
            operatorPriorityList.Add("sin", 1);
            operatorPriorityList.Add("cos", 1);
            operatorPriorityList.Add("tan", 1);
            operatorPriorityList.Add("~", 2);//this is the negative sign
            operatorPriorityList.Add("^", 3);
            operatorPriorityList.Add(".", 4);//this is the omitted multiply sign 
            operatorPriorityList.Add("*", 5);
            operatorPriorityList.Add("/", 5);
            operatorPriorityList.Add("+", 6);
            operatorPriorityList.Add("-", 6);
            //()
            //sqrt sin cos tan
            //+ - unary
            //^
            //.
            //* /
            //+ -
        }
        private bool isOperatorToken(string str)
        {
            if (str == "~") return true;
            if (str == "^") return true;
            if (str == ".") return true;
            if (str == "/") return true;
            if (str == "*") return true;
            if (str == "+") return true;
            if (str == "-") return true;
            return false;
        }
        private bool isFunctionToken(string str)
        {
            if (str == "sin") return true;
            if (str == "cos") return true;
            if (str == "tan") return true;
            if (str == "sqrt") return true;
            return false;
        }
        private bool isOperator(string str)
        {
            if (isOperatorToken(str)) return true;
            if (isFunctionToken(str)) return true;
            if (str == "(") return true;
            if (str == ")") return true;
            if (str == ",") return true;
            return false;
        }
        private int calc(string op, ref Stack<double> val) //0: success, 1: stack empty
        {
            double val1, val2;
            if (op == "sqrt")
            {
                val1 = val.Pop();
                val1 = Math.Sqrt(val1);
                val.Push(val1);
            }
            if (op == "sin")
            {
                val1 = val.Pop();
                val1 = Math.Sin(val1);
                val.Push(val1);
            }
            if (op == "cos")
            {
                val1 = val.Pop();
                val1 = Math.Cos(val1);
                val.Push(val1);
            }
            if (op == "tan")
            {
                val1 = val.Pop();
                val1 = Math.Tan(val1);
                val.Push(val1);
            }
            if (op == "~")
            {
                val1 = val.Pop();
                val1 = -val1;
                val.Push(val1);
            }
            if (op == "^")
            {
                val2 = val.Pop();
                val1 = val.Pop();
                val1 = Math.Pow(val1,val2);
                val.Push(val1);
            }
            if (op == ".")
            {
                val2 = val.Pop();
                val1 = val.Pop();
                val1 = val1 * val2;
                val.Push(val1);
            }
            if (op == "*")
            {
                val2 = val.Pop();
                val1 = val.Pop();
                val1 = val1 * val2;
                val.Push(val1);
            }
            if (op == "/")
            {
                val2 = val.Pop();
                val1 = val.Pop();
                val1 = val1 / val2;
                val.Push(val1);
            }
            if (op == "+")
            {
                val2 = val.Pop();
                val1 = val.Pop();
                val1 = val1 + val2;
                val.Push(val1);
            }
            if (op == "-")
            {
                val2 = val.Pop();
                val1 = val.Pop();
                val1 = val1 - val2;
                val.Push(val1);
            }
            return 0;
        }
        private bool okToAddOmitedMultiplySign(Object obj)
        {
            if (obj.GetType() == typeof(double)) return true;
            if (obj.GetType() == typeof(string))
            {
                string temp = (string)obj;
                if (temp == ")") return true;
                if (!isOperator(temp)) return true;
            }
            return false;
        }
        private void detectVar()
        {
            int detectedVarID = -1;
            for (int i = 0;i < variableVal.Length;++i)
                if (variableVal[i].First == 0)
                    if (detectedVarID == -1)
                    {
                        detectedVarID = i;
                        detectedVar = variableList2[detectedVarID];
                    }
                    else
                    {
                        throw new Exception("Có 2 ẩn số");
                    }
            if (detectedVarID == -1) throw new Exception("Không tìm thấy ẩn số");
            dynamicEps = 1;
            foreach (Pair<int,double> myelement in variableVal)
            {
                if (myelement.First != 0) dynamicEps = Math.Max(myelement.Second,dynamicEps);
            }
            dynamicEps = dynamicEps * 1e-8;
        }
        private bool checkDoubleError(double x)
        {
            if (double.IsNaN(x) || double.IsInfinity(x)) return true;
            return false;
        }
        public void inputExpression(string inputexpression)
        {
            if (initialized == false) Initializing();
            int i;
            int varcounter = 0;
            string reader;
            string tag;
            int dotnum;
            if (inputexpression == "")
            {
                expression = new Object[1];
                expression[0] = 0.0;
                return;
            }
            List<Object> tempexpression = new List<Object>();
            for (i = 0; i < inputexpression.Length;) //go from left to right
            {
                if (isalnum(inputexpression[i])) //is it a char or a num ? Yes => parse it, No => get the operator
                {
                    if (isanpha(inputexpression[i])) //is it a char ? Yes => parse the char
                    {
                        reader = "";
                        tag = "";
                        for (; i < inputexpression.Length; ++i) //copy the char
                            if (isanpha(inputexpression[i]))
                                reader += inputexpression[i];
                            else break;
                        if (i < inputexpression.Length && inputexpression[i] == '(')
                        {
                            if (reader.Length >= 3)
                            {
                                tag = reader.Substring(reader.Length - 3);
                                if (tag != "sin" && tag != "cos" && tag != "tan")
                                {
                                    tag = "";
                                }
                            }
                            if (tag == "" && reader.Length >= 4)
                            {
                                tag = reader.Substring(reader.Length - 4);
                                if (tag != "sqrt")
                                {
                                    tag = "";
                                }
                            }
                        }
                        if (tag != "") reader = reader.Remove(reader.Length - tag.Length);
                        if (reader.Length > 0)
                        {
                            for (int j = 0; j < reader.Length; ++j)
                            {
                                if (tempexpression.Count > 0 && okToAddOmitedMultiplySign(tempexpression[tempexpression.Count - 1]))
                                    tempexpression.Add(".");
                                tempexpression.Add(reader[j].ToString());
                                ++varcounter;
                            }
                        }
                        if (tag != "")
                        {
                            if (tempexpression.Count > 0 && okToAddOmitedMultiplySign(tempexpression[tempexpression.Count - 1]))
                                tempexpression.Add(".");
                            tempexpression.Add(tag);
                        }
                    }
                    else //parse the num
                    {
                        reader = "";
                        dotnum = 0;
                        for (;i < inputexpression.Length;++i) //copy the num
                            if (isdigit(inputexpression[i]))
                            {
                                if (inputexpression[i] == '.')
                                {
                                    ++dotnum;
                                    if (dotnum == 2)
                                        throw new Exception("Số có 2 dấu chấm");
                                }
                                reader += inputexpression[i];
                            }
                            else break;
                        tempexpression.Add(Double.Parse(reader));
                    }
                }
                else //a symbol
                {
                    if (inputexpression[i] == ' ') { } //ignore the spaces
                    else if (inputexpression[i] == '+' || inputexpression[i] == '-' || inputexpression[i] == '*' || inputexpression[i] == '/' ||
                        inputexpression[i] == '^' || inputexpression[i] == ')' || inputexpression[i] == ',')
                        tempexpression.Add(inputexpression[i].ToString()); //parse the operator
                    else if (inputexpression[i] == '(')
                    {
                        if ((tempexpression.Count > 0) && okToAddOmitedMultiplySign(tempexpression[tempexpression.Count - 1]))
                            tempexpression.Add(".");
                        tempexpression.Add("(");
                    }
                    else if (inputexpression[i] == '{') //parse the variable
                    {
                        reader = "";
                        ++i;
                        while (true)
                        {
                            if (i == inputexpression.Length) throw new Exception("Không tìm thấy dấu đóng ngoặc nhọn");
                            if (inputexpression[i] == '}') break;
                            reader += inputexpression[i];
                            ++i;
                        }
                        if ((tempexpression.Count > 0) && okToAddOmitedMultiplySign(tempexpression[tempexpression.Count - 1]))
                            tempexpression.Add(".");
                        if (reader == "pi" || reader == "Pi" || reader == "PI")
                        {
                            tempexpression.Add(Math.PI);
                        }
                        else
                        {
                            tempexpression.Add(reader);
                            ++varcounter;
                        }
                    }
                    else if (inputexpression[i] == '=') //this is equation
                    {
                        tempexpression.Add("-");
                        tempexpression.Add("(");
                        inputexpression += ")";
                        if (varcounter == 1) equationType = 5;
                    }
                    else if (inputexpression[i] == '<' || inputexpression[i] == '>') //this is non-equation
                    {
                        if (inputexpression[i] == '<')
                            if (inputexpression[i + 1] != '=') equationType = 1; else { equationType = 2; ++i; }
                        else
                            if (inputexpression[i + 1] != '=') equationType = 3; else { equationType = 4; ++i; }
                        tempexpression.Add("-");
                        tempexpression.Add("(");
                        inputexpression += ")";
                    }
                    else if (inputexpression[i] == 'π')
                    {
                        if ((tempexpression.Count > 0) && okToAddOmitedMultiplySign(tempexpression[tempexpression.Count - 1]))
                            tempexpression.Add(".");
                        tempexpression.Add(Math.PI);
                    }
                    else
                    {
                        throw new Exception("Ký tự không hợp lệ");
                    }
                    ++i;
                }
            }
            expression = new Object[tempexpression.Count];
            for (i = 0; i < tempexpression.Count; ++i) expression[i] = tempexpression[i];
            //makes a list of variables
            variableList = new Dictionary<string, int>();
            varcounter = 0;
            for (i = 0; i < expression.Length; ++i)
            {
                if (expression[i].GetType() == typeof(string))
                {
                    reader = (string) expression[i];
                    if (!isOperator(reader))
                        if (!variableList.ContainsKey(reader))
                        {
                            variableList[reader] = varcounter;
                            ++varcounter;
                        }
                }
            }
            variableList2 = new string[variableList.Count];
            variableVal = new Pair<int, double>[variableList.Count];
            foreach (KeyValuePair<string, int> k in variableList)
            {
                variableList2[k.Value] = k.Key;
                variableVal[k.Value] = new Pair<int, double>(0, 0.0);
            }
        }
        public void addVariable(string var,double num)
        {
            if (!variableList.ContainsKey(var)) return;//it should return if it doesn't contain that var
            int i = variableList[var];
            variableVal[i].First = 1;
            variableVal[i].Second = num;
        }
        public void replaceVariableSystem(Dictionary<string,int> addVariableList,Pair<int,double>[] addVariableVal)
        {
            int destination,from;
            foreach (KeyValuePair<string, int> myVariable in variableList)
            {
                destination = myVariable.Value;
                from = addVariableList[myVariable.Key];
                if (addVariableVal[from].First == 1)
                {
                    variableVal[destination].First = 1;
                    variableVal[destination].Second = addVariableVal[from].Second;
                }
                else
                {
                    variableVal[destination].First = 0;
                }
            }
        }
        public void updateVariable(string var, double num)
        {
            int i = variableList[var];
            variableVal[i].Second = num;
        }
        public void removeVariable(string var)
        {
            int i = variableList[var];
            variableVal[i].First = 0;
        }
        public string LinearSyntax()
        {
            string result = "";
            for (int i = 0; i < expression.Length; ++i) result += expression[i];
            return result;
        }
        public double getResult()
        {
            int i,j,factor;
            Stack<string> opstack = new Stack<string>();
            Stack<double> valstack = new Stack<double>();
            string reader,plusminusreader;
            string opStackToken;
            for (i = 0;i < expression.Length;++i)
            {
                if (expression[i].GetType() == typeof(double)) valstack.Push((Double)expression[i]);
                else
                {
                    reader = (string)expression[i];
                    if (isOperator(reader))
                    {
                        if (isFunctionToken(reader) && operatorPriorityList[reader] == 1) opstack.Push(reader);//If the token is a function token, then push it onto the stack.
                        else if (reader == ",")
                        {
                            if (opstack.Count == 0) throw new Exception("Dấu phẩy đặt sai chỗ");
                            while (opstack.Peek() != "(")
                            {
                                if (opstack.Count == 0) throw new Exception("Dấu phẩy đặt sai chỗ");
                                calc(opstack.Pop(), ref valstack);
                            }
                        }
                        else if (reader == "(")
                        {
                            opstack.Push(reader);
                        }
                        else if (reader == ")")
                        {
                            if (opstack.Count == 0) throw new Exception("Sai ngoặc");
                            while (opstack.Peek() != "(")
                            {
                                if (opstack.Count == 0) throw new Exception("Sai ngoặc");
                                calc(opstack.Pop(), ref valstack);
                            }
                            opstack.Pop();
                            opStackToken = opstack.Peek();
                            if (opStackToken == "sqrt" || opStackToken == "sin" || opStackToken == "cos" || opStackToken == "tan")
                            {
                                opstack.Pop();
                                calc(opStackToken,ref valstack);
                            }
                        }
                        else if (reader == "+" || reader == "-") //xử lý + - riêng cho TH dấu âm dương (+ - mang 2 ý nghĩa: unary operator & binary operator)
                        {
                            j = i - 1;
                            factor = 1;
                            for (; i < expression.Length; ++i)
                                {
                                    if (expression[i].GetType() == typeof(string))
                                    {
                                        plusminusreader = (string)expression[i];
                                        if (plusminusreader == "-") factor *= -1;
                                        if (plusminusreader != "-" && plusminusreader != "+") { --i; break; }
                                    }
                                    else { --i; break; }
                                }
                            if (j == -1)
                            {
                                if (factor == -1) opstack.Push("~");
                            }
                            else
                            {
                                if (expression[j].GetType() == typeof(string))
                                {
                                    plusminusreader = (string) expression[j];
                                    if (plusminusreader == "^" || plusminusreader == "." || plusminusreader == "(" ||
                                        plusminusreader == "*" || plusminusreader == "/" || plusminusreader == ",")
                                    {
                                        if (factor == -1) opstack.Push("~");
                                    }
                                    else
                                    {
                                        if (opstack.Count != 0)
                                        {
                                            opStackToken = opstack.Peek();
                                            while (isOperatorToken(opStackToken) && (operatorPriorityList[opStackToken] <= operatorPriorityList["+"]))
                                            {
                                                calc(opstack.Pop(), ref valstack);
                                                if (opstack.Count != 0) opStackToken = opstack.Peek(); else break;
                                            }
                                        }
                                        if (factor == 1) opstack.Push("+"); else opstack.Push("-");
                                    }
                                }
                                else
                                {
                                    if (opstack.Count != 0)
                                    {
                                        opStackToken = opstack.Peek();
                                        while (isOperatorToken(opStackToken) && (operatorPriorityList[opStackToken] <= operatorPriorityList["+"]))
                                        {
                                            calc(opstack.Pop(), ref valstack);
                                            if (opstack.Count != 0) opStackToken = opstack.Peek(); else break;
                                        }
                                    }
                                    if (factor == 1) opstack.Push("+"); else opstack.Push("-");
                                }
                            }
                        }
                        else
                        {
                            if (opstack.Count != 0)
                            {
                                opStackToken = opstack.Peek();
                                while (isOperatorToken(opStackToken) && (operatorPriorityList[opStackToken] <= operatorPriorityList[reader]))
                                {
                                    calc(opstack.Pop(), ref valstack);
                                    if (opstack.Count != 0) opStackToken = opstack.Peek(); else break;
                                }
                            }
                            opstack.Push(reader);
                        }
                    }
                    else
                    {
                        //reader is not an operator, find the value of it and push it to stack
                        valstack.Push(variableVal[variableList[reader]].Second);
                    }
                }

            }
            while (opstack.Count != 0)
            {
                if (opstack.Peek() == "(")
                    throw new Exception("Sai ngoặc");
                calc(opstack.Pop(), ref valstack);
            }
            return valstack.Pop();
        }
        public double getResult_safe(ref int err)
        {
            double res;
            try
            {
                res = getResult();
            }
            catch (Exception)
            {
                err = 1;
                return 0.0;
            }
            return res;
        }
        public double calcX_safe(double x, ref int err)
        {
            double res;
            addVariable(detectedVar, x);
            res =  getResult_safe(ref err);
            removeVariable(detectedVar);
            return res;
        }
        public double getDerivative_safe(double x,ref int err)
        {
            double mdEps = 1e-8;
            double mdEps2 = 0.0000000410125;
            double res;
            double fx, fxd1, fxd2, fx_d1, fx_d2, dhl, dhr, dvhl, dvhr;
            int err0, err1 = 0, err2 = 0, err3 = 0, err4 = 0, ehl, ehr, evhl, evhr;
            fx = calcX_safe(x, ref err);
            if (err != 0) return x;
            fxd1 = calcX_safe(x + mdEps,ref err1);
            fxd2 = calcX_safe(x + mdEps + mdEps, ref err2);
            fx_d1 = calcX_safe(x - mdEps, ref err3);
            fx_d2 = calcX_safe(x - mdEps - mdEps, ref err4);
            dhl = (4 * fx_d1 - fx_d2 - 3 * fx) / (2 * mdEps);
            dhr = (4 * fxd1 - fxd2 - 3 * fx) / (2 * mdEps);
            dvhl = (fx_d1 - fx) / mdEps;
            dvhr = (fxd1 - fx) / mdEps;
            err0 = err1 + err2 + err3 + err4;
            ehl = err3 + err4;
            ehr = err1 + err2;
            evhl = err3;
            evhr = err1;
            if (err0 == 0) res = (8 * fxd1 - 8 * fx_d1 + fx_d2 - fxd2) / (12 * mdEps);
   	        else
	        {
   		        fxd1 = calcX_safe(x + mdEps2,ref err1);
		        fxd2 = calcX_safe(x + mdEps2 + mdEps,ref err2);
		        fx_d1 = calcX_safe(x - mdEps2,ref err3);
		        fx_d2 = calcX_safe(x - mdEps2 - mdEps2,ref err4);
		        err0 = err1 + err2 + err3 + err4;
		        if (err0 == 0) res = (8 * fxd1 - 8 * fx_d1 + fx_d2 - fxd2) / (12 * mdEps2);
		        else
		        {
			        if (ehr == 0) res = dhr;
			        else if (ehl == 0) res = dhl;
			        else if (evhr == 0) res = dvhr;
			        else if (evhl == 0) res = dvhl; else {err = 2;res = x;}
		        }
	        }
	        return res;
        }
        public int Solve(double x,ref double res)
        {
            const int meXNUM = 24;
            const int meMaxRCSolve = 20;
            const double meEEps = 1e-14;
            int i, RC, f1, f2, err = 0;
            double yy,y,dy;
            double[] meXsolve = new double[26]{0.0,0.0,0.5,-0.5,1.0,-1.0,1.5,-1.5,2.0,-2.0,2.14,-2.14,3.14,-3.14,7.1,-7.1,10.02,-10.02,10e+12,10e-12,-10e+12,-10e-12,1e+99,1e-99,-1e+99,-1e-99};
            detectVar();
            meXsolve[0] = x;
            for (i = 0;i < meXNUM;++i)
            {
                x = y = meXsolve[i];
                RC = 0;
                while (true)
                {
                    yy = x;
                    x = y;
                    y = calcX_safe(x,ref err);
                    if (err == 2) break; else if (err != 0) throw new Exception("Lỗi biểu thức");
                    dy = getDerivative_safe(x, ref err);
                    if (err == 2) break; else if (err != 0) throw new Exception("Lỗi biểu thức");
                    //if (mAbs(dy) <= mLimitNumUnderflow) break;
                    y = x - (y / dy);
                    if (checkDoubleError(y)) break;
                    if (Math.Abs(y - x) <= Math.Abs(y * meEEps)) // a == b ?
                    {
                        if (x >= dynamicEps)
                        {
                            res = x;
                            return 1;
                        }
                        else break;
                    }
                    if (x > yy) f1 = 1; else f1 = 0;
                    if (y > x) f2 = 1; else f2 = 0;
                    if (f1 != f2) ++RC;
                    if (RC > meMaxRCSolve) break;
                }
            }
            return 0;
        }
        public int SolveInRange(double x, double lower,double upper,int rangetype,ref double res)
        {
            const int meXNUM = 24;
            const int meMaxRCSolve = 20;
            const double meEEps = 1e-14;
            int i, RC, f1, f2, err = 0;
            double yy, y, dy;
            double[] meXsolve = new double[26] { 0.0, 0.0, 0.5, -0.5, 1.0, -1.0, 1.5, -1.5, 2.0, -2.0, 2.14, -2.14, 3.14, -3.14, 7.1, -7.1, 10.02, -10.02, 10e+12, 10e-12, -10e+12, -10e-12, 1e+99, 1e-99, -1e+99, -1e-99 };
            detectVar();
            meXsolve[0] = x;
            for (i = 0; i < meXNUM; ++i)
            {
                x = y = meXsolve[i];
                RC = 0;
                while (true)
                {
                    yy = x;
                    x = y;
                    y = calcX_safe(x, ref err);
                    if (err == 2) break; else if (err != 0) throw new Exception("Lỗi biểu thức");
                    dy = getDerivative_safe(x, ref err);
                    if (err == 2) break; else if (err != 0) throw new Exception("Lỗi biểu thức");
                    //if (mAbs(dy) <= mLimitNumUnderflow) break;
                    y = x - (y / dy);
                    if (checkDoubleError(y)) break;
                    if (Math.Abs(y - x) <= Math.Abs(y * meEEps)) // a == b ?
                    {
                        if (isInRange(x, lower, upper, rangetype))
                        {
                            res = x;
                            return 1;
                        }
                        else break;
                    }
                    if (x > yy) f1 = 1; else f1 = 0;
                    if (y > x) f2 = 1; else f2 = 0;
                    if (f1 != f2) ++RC;
                    if (RC > meMaxRCSolve) break;
                }
            }
            return 0;
            //binary search
            //double ylower, yupper;
            //switch (rangetype)
            //{
            //    case 0:
            //    case 1: upper = Double.MaxValue;break;
            //    case 2:
            //    case 3: lower = Double.MinValue;break;
            //}
            //x = (lower + upper) / 2;
            //ylower = calcX_safe(lower, ref err);
            //yupper = calcX_safe(upper, ref err);

            //while (true) //~~~~????
            //{
            //    y = calcX_safe(x, ref err);
            //    if (err == 2) throw new Exception("Biểu thức không liên tục"); else if (err != 0) throw new Exception("Lỗi biểu thức");
            //    /*TEMP CODE
            //        double mFindx(double* f, int n, double xlower, double xupper, double fxlower, double fxupper)
            //        {
            //            double res, fx, fxlast;
            //            if (fxlower > 0.0)
            //            {
            //                res = fxlower; fxlower = fxupper; fxupper = res;
            //                res = xlower; xlower = xupper; xupper = res;
            //            }
            //            fxlast = mCalcf(f, n, xlower);
            //            do
            //            {
            //                res = (xlower + xupper) / 2;
            //                fx = mCalcf(f, n, res);
            //                //if (mAbs(fx) < mEps) break;
            //                if (fx == fxlast) break;
            //                else if (fx < 0.0) xlower = res;
            //                else xupper = res;
            //                fxlast = fx;
            //            }
            //            while (1);
            //            if (mAbs(res) < mEps) return 0.0;
            //            return res;
            //        }
            //     * */
            //}
        }

        private bool isInRange(double x, double lower, double upper, int rangetype)//0 > lower, 1 >= lower, 2 < upper, 3 <= upper, lower < 4 < upper, lower <= 5 <= upper
        {
            switch (rangetype)
            {
                case 0: if (x > lower + dynamicEps) return true; else return false;
                case 1: if (x >= lower + dynamicEps) return true; else return false;
                case 2: if (x < upper) return true; else return false;
                case 3: if (x <= upper) return true; else return false;
                case 4: if (x > lower + dynamicEps && x < upper) return true; else return false;
                case 5: if (x >= lower + dynamicEps && x <= upper) return true; else return false;
                default: throw new Exception("Loại so sánh không hợp lệ");
            }
        }
        public bool getTrueFalse()
        {
            double val = getResult();
            switch (equationType)
            {
                case 1: if (val < 0) return true; else return false;
                case 2: if (val <= 0) return true; else return false;
                case 3: if (val > 0) return true; else return false;
                case 4: if (val >= 0) return true; else return false;
            }
            return false;
        }
        public double quickGetResult(string str)
        {
            inputExpression(str);
            return getResult();
        }
    }
    public class Pair<T, U>
    {
        public Pair(){}
        public Pair(T first, U second){this.First = first;this.Second = second;}
        public T First { get; set; }
        public U Second { get; set; }
    };
}
