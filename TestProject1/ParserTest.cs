﻿using AITriangleWPF;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject1
{
    
    
    /// <summary>
    ///This is a test class for ParserTest and is intended
    ///to contain all ParserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ParserTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for inputExpression
        ///</summary>
        ///

        [TestMethod()]
        public void getResultTestGeneral()
        {
            Parser target = new Parser();
            string inputexpression = "9*15+2*3abcsin(15)+2{ma}{da}aa+-sqrt(16)^-+-+-2";
            double expected = 12702616.924414947352905163058041;
            double actual;
            target.inputExpression(inputexpression);
            //string test = target.LinearSyntax();
            target.addVariable("a", 55);
            target.addVariable("b", 22);
            target.addVariable("c", 11);
            target.addVariable("ma", 17);
            target.addVariable("da", 123);
            actual = target.getResult();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getResultTestSimplePlusMinus()
        {
            Parser target = new Parser();
            string inputexpression = "-1+2+3+4";
            double expected = 8;
            double actual;
            target.inputExpression(inputexpression);
            actual = target.getResult();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getResultTestMorePlusMinus()
        {
            Parser target = new Parser();
            string inputexpression = "+-sqrt(16)^-+-+-2";
            double expected = 0.0625;
            double actual;
            target.inputExpression(inputexpression);
            actual = target.getResult();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getResultTestPi()
        {
            Parser target = new Parser();
            string inputexpression = "-1+2{pi}sin(5)+3*4";
            double expected = 4.9749010867387261656191666161518;
            double actual;
            target.inputExpression(inputexpression);
            actual = target.getResult();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getResultTestNullTest()
        {
            Parser target = new Parser();
            string inputexpression = "";
            double expected = 0.0;
            double actual;
            target.inputExpression(inputexpression);
            actual = target.getResult();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getResultTestSQRT()
        {
            Parser target = new Parser();
            string inputexpression = "sqrt(6(6-3)(6-4)(6-5))";
            double expected = 6;
            double actual;
            target.inputExpression(inputexpression);
            actual = target.getResult();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void solveFXTest1()
        {
            Parser target = new Parser();
            string inputexpression = "{hA} = bsin(C)";
            double expected = 4;
            double actual = 3333333333;
            target.inputExpression(inputexpression);
            target.addVariable("b", 4);
            target.addVariable("C", Math.PI / 2);
            target.Solve(0, ref actual);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void solveFXTest2()
        {
            Parser target = new Parser();
            string inputexpression = "4{mC}^2-(2a^2+2b^2-c^2)";
            int expected = 0;
            int actual = 0;
            double dummy = 0;
            target.inputExpression(inputexpression);
            target.addVariable("a", 3);
            target.addVariable("b", 4);
            target.addVariable("c", 15);
            actual = target.Solve(0, ref dummy);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void solveFXTest3()
        {
            Parser target = new Parser();
            string inputexpression = "cos(A) = cos(2{pi}/3)";
            double expected = Math.PI * 2.0 / 3;
            double actual = 0;
            target.inputExpression(inputexpression);
            target.SolveInRange(0,0,Math.PI,4, ref actual);
            Assert.AreEqual(expected, actual);
        }

    }
}
